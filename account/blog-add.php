<?php

require_once 'core/init.php';

$super_user = $login = false;
    if(cek_level($_SESSION['user']) == 1){
        $super_user = true;

    }else{
        header('Location: index.php');
    }
       $error ='';
       if(isset($_REQUEST['submit'])){
           $judul = $_POST['judul'];
           $konten = $_POST['konten'];
            $tag = $_POST['tag'];
            $kategori = $_POST['kategori'];
            $sort = $_POST['sort'];
            $user_post = $_SESSION['user'];
            $videoke = $_POST['videoke'];
            $poin_per_video = $_POST['poin_per_video'];


            /** Files **/

             $time = time();
             $nama = $_FILES['video']['name'];
             $error = $_FILES['video']['error'];
             $size = $_FILES['video']['size'];
             $asal = $_FILES['video']['tmp_name'];
             $format = $_FILES['video']['type'];
             $namafile = 'upload/video/'. $nama;
             

             if($error == 0 ) {
                if($size < 77000000){
                   if($format == 'video/mp4' || 'video/ogg' || 'video/webm' || 'video/3gpp' || 'video/mov' || 'video/x-flv' || 'video/quicktime') {
                      if(file_exists($namafile)){
                            $error = 'Nama File sudah ada, Ganti nama file dulu';
                       } else {

                        if(!empty(trim($judul)) && !empty(trim($konten))){
                            if(tambah_data($namafile, $judul, $konten, $tag, $kategori, $sort, $user_post, $videoke, $poin_per_video)){
                                move_uploaded_file($asal, $namafile);
                                move_uploaded_file($asal, 'upload/video/'. $nama);
                                header('location: blog.php');
                            }else{
                                $error='ada masalah saat menambah data';
                            }

                        }else{
                          $error = 'judul dan konten wajib diisi';
                        }
                        }
                      } else {
                      $error = 'formatnya harus mp4 / webm / ogg';
                    }
                  }else{
                   $error = 'Video Size Minimal adalah 64MB';
                 }
              }else{
                $error = 'ada error';
              }
           


    }

require_once "view/header.php";
?>

        <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            Upload Video
                            <small></small>
                        </h1>
                        <ol class="breadcrumb">
                            <li>
                                <i class="fa fa-dashboard"></i>  <a href="index.php">Dashboard</a>
                            </li>
                            <li class="active">
                                <i class="fa fa-book"></i> Upload video
                            </li>
                        </ol>

                        <form class="form-group" action="" method="post" enctype="multipart/form-data">
                            <div class="col-md-12">
                                <div class="col-md-6">
                                     <div class="form-group">
                                        <label for="Judul">Judul</label>
                                        <input type="text" class="form-control" name="judul">
                                     </div>
                                     <div class="form-group">
                                         <label for="Tag">Tag</label>
                                        <input type="text" class="form-control" name="tag">
                                     </div>
                                     <div class="form-group">
                                         <label for="Tag">Video Ke (* Untuk Urutan Video Utama / Dengan Point</label>
                                        <input type="number" class="form-control" name="videoke">
                                     </div>
                                     <div class="form-group">
                                         <label for="Tag">Point Per Video</label>
                                        <input type="number" class="form-control" name="poin_per_video">
                                     </div>
                                     <div class="form-group">
                                         <select name="kategori" class="form-control">
                                             <option value="0">Halaman Depan</option>
                                             <option value="1">Aktivitas</option>
                                             <option value="2">Utama (Dengan Point)</option>
                                         </select>
                                     </div>
                                  </div>
                                  <div class="col-md-6">
                                    <div class="form-group">
                                         <label for="Konten">Konten</label>
                                         <textarea name="konten" class="form-control" rows="8" cols="40"></textarea>
                                    </div>
                                  </div>
                                  <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="nama" class="col-md-2 control-label">Upload File</label>
                                          <div class="col-md-4">
                                            <input type="file" name="video" required>
                                          </div>
                                           
                                    </div>

                                  </div>

                                 <div id="error"><?= $error ?></div><br>
                                 <button type="submit" name="submit" class="btn btn-success">Submit</button>
                                 <label class="control-label">*Ukuran video maksimal adalah 60 Mb</label>
                            </div>
                        </form>

                    </div>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->
<?php

require_once "view/footer.php";

?>
