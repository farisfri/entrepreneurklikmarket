<?php
require_once "core/init.php";
//proteksi
if(!isset($_SESSION['user'])) {
		header('Location: login.php');
}

$super_user = $login = false;
    if(cek_level($_SESSION['user']) == 1){
        $super_user = true;

    }else{
        header('Location: index.php');
    }

$error ='';
$id = $_GET['id'];

if(isset($_GET['id'])){

	$article =tampilkan_per_id($id);
	while($row=mysqli_fetch_assoc($article)){
		$judul_awal = $row['judul'];
		$isi_awal = $row['isi'];
		$tag_awal = $row['tag'];
        $kategori_awal = $row['kategori'];
        $videoke = $row['videoke'];
        $poin_per_video = $row['poin_per_video'];
	}
}

if(isset($_POST['submit'])){
	$judul = $_POST['judul'];
	$konten = $_POST['konten'];
	$tag = $_POST['tag'];
    $kategori = $_POST['kategori'];
    $videoke = $_POST['videoke'];
    $poin_per_video = $_POST['poin_per_video'];

	if(!empty(trim($judul)) && !empty(trim($konten))){
		if(edit_data($judul, $konten, $tag, $kategori, $videoke, $poin_per_video, $id)){
			header('location: blog.php');
		}else{
			$error='ada masalah saat update data';
		}

	}else{
		$error = 'judul dan konten wajib diisi';
	}
}
require_once "view/header.php";
?>

 <div class="content">
            <div class="container-fluid">
               <div class="row">
                 <div class="card">
                        <div class="header">
                            <h4 class="title">Edit Video</h4>
                            <p class="category"></p>
                        </div>

                        <div class="content">
                            <div class="container">
                        <form class="form-group" action="" method="post">
                            <div class="col-md-12">
                                <div class="col-md-6">
                                     <div class="form-group">
                                        <label for="Judul">Judul</label>
                                        <input type="text" class="form-control" name="judul" value="<?=$judul_awal; ?>">
                                     </div>
                                     <div class="form-group">
                                         <label for="Tag">Tag</label>
                                        <input type="text" class="form-control" name="tag" value="<?=$tag_awal; ?>">
                                     </div>
                                     <div class="form-group">
                                         <label for="Tag">Video Ke (* Untuk Urutan Video Utama / Dengan Point</label>
                                        <input type="number" class="form-control" name="videoke" value="<?= $videoke ?>">
                                     </div>
                                     <div class="form-group">
                                         <label for="Tag">Point Per Video</label>
                                        <input type="number" class="form-control" name="poin_per_video" value="<?= $poin_per_video ?>">
                                     </div>
                                     <div class="form-group">
                        				 <select name="kategori" class="form-control">
                            				 <option value="0" <?php if($kategori_awal == '0'){ echo 'selected'; } ?>>Halaman Depan</option>
                            				 <option value="1" <?php if($kategori_awal == '1'){ echo 'selected'; } ?>>Aktivitas</option>
                            				 <option value="2" <?php if($kategori_awal == '2'){ echo 'selected'; } ?>>Utama</option>
    
                        				 </select>
                                     </div>
                                  </div>
                                  <div class="col-md-6">
                        			<!-- <div class="form-group">
                        					<label for="Tag">Sort</label>
                        				 <input type="text" class="form-control" name="sort" value="<?=$sort; ?>">
                        			</div> -->
                                    <div class="form-group">
                                         <label for="Konten">Konten</label>
                                         <textarea name="konten" class="form-control" rows="8" cols="40"><?=$isi_awal; ?></textarea>
                                    </div>
                                  </div>
                                 <div id="error"><?= $error ?></div><br>
                                 <button type="submit" name="submit" class="btn btn-success">Submit</button>
                            </div>
                        </div>
                        </form>
                    </div>

                    </div>
                </div>
            </div>
</div>




<?php

require_once "view/footer.php";

?>
