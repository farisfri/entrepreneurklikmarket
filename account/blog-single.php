<?php 
require_once "core/init.php";
require_once "view/header.php";
    
$super_user = $login = false;
    if(cek_level($_SESSION['user']) == 1){
        $super_user = true;

    }else{
        header('Location: index.php');
    }

$error ='';
$id = $_GET['id'];

if(isset($_GET['id'])){

	$article =tampilkan_per_id($id);
	while($row=mysqli_fetch_assoc($article)){
		$judul_awal = $row['judul'];
		$isi_awal = $row['isi'];
		$tag_awal = $row['tag'];
        $waktu = $row['waktu'];
	}
}
?>

<div class="content">
            <div class="container-fluid">
               <div class="row">
                 <div class="card">
                        <div class="header">
                            <h4 class="title"><?= $judul_awal; ?></h4>
                            <p class="category"></p>
                        </div>

                        <div class="content">
                            <div class="container">
                            
                            <small>by </small>
                        </h1>
                        <ol class="breadcrumb">
                            <li>
                                <i class="fa fa-dashboard"></i>  <a href="index.php">Dashboard</a>
                            </li>
                            <li class="active">
                                <i class="fa fa-book"></i> Single Blog
                            </li>
                        </ol>
            <hr>

                <!-- Date/Time -->
                <p><span class="glyphicon glyphicon-time"></span> Posted on <?= $waktu; ?></p>
                <p><span class="glyphicon glyphicon-tag"></span> <?= $tag_awal; ?></p>
                <hr>
            

                <!-- Post Content -->
                <p><?= $isi_awal; ?></p>
            
                </div>

</div>
</div>
</div>
</div>


<?php 

require_once "view/footer.php";
    
