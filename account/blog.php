<?php

require_once 'core/init.php';

 $articles =tampilkan();

//set session true/false

$super_user = $login = false;
    if(cek_level($_SESSION['user']) == 1){
        $super_user = true;
    }else{
        header('Location: index.php');
    }





if (isset($_GET['cari'])) {

    $cari = $_GET['cari'];

    $articles =hasil_cari($_GET['cari']);

}



//Pagination

$perPage = 5000; //perhalaman
$page = isset($_GET["halaman"]) ? (int)$_GET["halaman"] : 1;
$start = ($page > 1) ? ($page * $perPage) - $perPage : 0;
$articles = "SELECT * FROM blog LIMIT $start, $perPage";
$result2 = mysqli_query($link, $articles);
$result = mysqli_query($link, "SELECT * FROM blog");
$total = mysqli_num_rows($result);
$pages = ceil($total/$perPage);

//end of Pagination

require_once "view/header.php";
?>
        <div id="page-wrapper">
            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
<!--

<form action"" method="get">

    <input type="search" name="cari" placeholder="Cari">

</form>-->

<?php if($super_user == true): ?>
            <br> &nbsp
                    <?php endif; ?>
                        <div class="card">
                            <div class="header">
                                <h4 class="title">Video</h4>
                                <p class="category"></p>
                                <a href="blog-add.php"><button class="btn btn-primary tombol">+ Tambah Video Baru</button></a>
                            </div>
                            <div class="content table-responsive">
                                <table class="table table-striped display nowrap" id="dataVide">
                                    <thead>
                                        <tr>
                                            <th>Opsi</th>
                                            <th>No.</th>
                                            <!--<th>Gambar</th>-->
                                            <th>Judul</th>
                                            <th>Waktu</th>
                                            <th>Isi</th>
                                            <th>Tag</th>
                                            <th>Kategori Artikel</th>
                                           <!--  <th>Urutan</th> -->
                                            <th>Video Ke</th>
                                            
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php while($row=mysqli_fetch_assoc($result2)):  ?>
                                        <tr>
                                         <td><!--proteksi-->
                                             <?php if($super_user == true): ?>

                                                <a class="btn btn-primary" href="blog-edit.php?id=<?= $row['id']; ?>"> <i class="ti-pencil-alt"></i> </a>
                                                <a class="btn btn-danger" href="blog-delete.php?id=<?= $row['id']; ?>" onclick="return confirm('Anda yakin mau menghapus item ini ?')"> <i class="ti-trash"></i> </a>

                                             <?php endif; ?></td>
                                            <td><?= $row['id'];?></td>
                                            <!--<td width="30%"><img width="30%" src="<?= $row['nama_file'];?>"></td>-->
                                            <td><!--<a href="blog-single.php?id=<?= $row['id'];?>">--> <?= $row['judul']; ?></a></td>
                                            <td><?= $row['waktu']; ?></td>
                                            <td><?= excerpt($row['isi']); ?></td>
                                            <td><?= $row['tag']; ?></td>
                                             <td><?php
                                                         if($row['kategori'] == 0){
                                                             echo "Halaman Depan";
                                                         }elseif($row['kategori'] == 1){
                                                             echo "Aktivitas";
                                                         }elseif($row['kategori'] == 2){
                                                             echo "Utama";
                                                         }
                                                         ?></td>
                                                        <!-- <td><?= $row['sort']; ?></td> -->
                                                        <td><?= $row['videoke']; ?></td>
                                           
                                            <?php endwhile; ?>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>

                            <!-- Pagination -->

                            <?php while ($row = mysqli_fetch_assoc($result2)) { ?>

                                <!--<p><?php echo $row["judul"] ?></p>

                                <p><?php echo $row["isi"] ?></p>-->

                            <?php } ?>

                            <nav aria-label="Page navigation">
                              <ul class="pagination">
                                <?php for($i=1; $i<=$pages; $i++){ ?>
                                <li><a href="?halaman=<?php echo $i?>"><?php echo $i?></a></li>
                                </a> <?php } ?>
                              </ul>
                            </nav>

                        </div>
                </div>

                <!-- /.row -->
            </div>

            <!-- /.container-fluid -->

        </div>

        <!-- /#page-wrapper -->




<?php

require_once "view/footer.php";

?>

