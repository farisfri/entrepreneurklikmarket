         <?php
         require_once "core/init.php";



         $infodown =tampilFile();
         $filedown = mysqli_query($link, "SELECT * FROM file WHERE kategori = '5' ORDER BY sort ASC");
         $lifetime = mysqli_query($link, "SELECT * FROM blog WHERE kategori = '5' ORDER BY sort ASC");
         require_once "view/header.php";
         ?>


         <div class="content">
            <div class="container-fluid">
               <div class="row">

                    <div class="col-lg-8 col-md-7">
                        <?php while($row=mysqli_fetch_assoc($lifetime)):  ?>
                            <div class="card card-vid" style="width: 300px; height: 180px;">
                                <div class="header">
                                    <h4 class="title"><?= excerptJudul($row['judul']);?></h4>
                                </div>
                                <div class="content embed-responsive embed-responsive-16by9" style="">
                                    <video class="embed-responsive-item mejs__player" style="width: 300px; height: 180px;" controls="controls" preload="none" preload="none" controls playsinline webkit-playsinline>
                                        <source type="video/mp4" src="<?= $row['nama_file'];?>" />
                                        <source type="video/webm" src="<?= $row['nama_file'];?>" />
                                        <source type="video/ogg" src="<?= $row['nama_file'];?>" />
                                        <object width="270" height="180" type="application/x-shockwave-flash" data="/player/build/mediaelement-flash-video.swf">
                                            <param name="movie" value="/player/build/mediaelement-flash-video.swf" />
                                            <param name="flashvars" value="controls=true&amp;poster=#&amp;file=<?= $row['nama_file'];?>" />
                                            <img src="#" width="300" height="180" title="No video playback capabilities" />
                                        </object>

                                    </video>
                                            </div>
                            </div>
                        <?php endwhile; ?>
                    </div>

                    <div class="col-lg-4 col-md-5">
                      <div class="list-group text-center">
                            <a href="#" class="list-group-item list-group-item-action">
                                PDF
                            </a>
                            <?php while($row=mysqli_fetch_assoc($filedown)):  ?>
                              <a href="<?= $row['nama_file']; ?>" class="list-group-item list-group-item-action" download>
                                <?= $row['judul']; ?>
                              </a>
                            <?php endwhile; ?>
                          </div>
                    </div>
               </div>
            </div>
        </div>


<?php
require_once "view/footer.php"; ?>
