<?php 

require_once "core/init.php";

//proteksi

if(!isset($_SESSION['user'])) {
        header('Location: login.php');
}

$super_user = $login = false;
    if(cek_level($_SESSION['user']) == 1){
        $super_user = true;
    }else{
        header('Location: index.php');
    }  
$error ='';

$id = $_GET['id'];

if(isset($_GET['id'])){

    $article = lihat_per_coach($id);

    while($row=mysqli_fetch_assoc($article)){

        $nama = $row['nama'];

        $sort = $row['sort'];

        $kota = $row['kota'];

    }

}



if(isset($_POST['submit'])){

    $nama = $_POST['nama'];

    $sort = $_POST['sort'];

    $kota = $_POST['kota'];



    if(!empty(trim($nama)) && !empty(trim($kota))){

        if(edit_coach($nama, $sort, $kota, $id)){

            header('location: coach.php');

        }else{

            $error='ada masalah saat update data';

        }



    }else{

        $error = 'judul dan konten wajib diisi';

    }

}

require_once "view/header.php";

?>



<div id="page-wrapper">



            <div class="container-fluid">



                <!-- Page Heading -->

                <div class="row">

                    <div class="col-lg-12">

                        <h1 class="page-header">

                            Edit Coach

                            <small></small>

                        </h1>





<form class="form-group" action="" method="post">

    <div class="col-md-12">

        <div class="col-md-6">

             <div class="form-group">

                <label for="Judul">nama</label>

                <input type="text" class="form-control" name="nama" value="<?=$nama; ?>">

             </div>

             <div class="form-group">

                 <label for="Tag">Urutan</label>

                <input type="text" class="form-control" name="sort" value="<?=$sort; ?>">

             </div>

             <div class="form-group">

                 <label for="Tag">Kota</label>

                <input type="text" class="form-control" name="kota" value="<?=$kota; ?>">

             </div>

             <div id="error"><?= $error ?></div><br>

         <button type="submit" name="submit" class="btn btn-success">Submit</button>

         <br>

          </div>



         

    </div>

</form>



</div>

</div>

</div>

</div>



<?php 



require_once "view/footer.php";

    

?>