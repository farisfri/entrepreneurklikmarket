<?php 

require_once 'core/init.php';

$super_user = $login = false;
    if(cek_level($_SESSION['user']) == 1){
        $super_user = true;

    }else{
        header('Location: index.php');
    }

                    $error ='';
                    if(isset($_POST['submit'])){
                        $namec = $_POST['namec'];
                        $kota = $_POST['kota'];
                        $sort = $_POST['sort'];

                        $time = time(); 
                        $nama = $_FILES['gambar']['name'];
                        $error = $_FILES['gambar']['error'];
                        $size = $_FILES['gambar']['size'];
                        $asal = $_FILES['gambar']['tmp_name'];
                        $format = $_FILES['gambar']['type'];
                        $namafile = 'upload/coach/'. $nama;
    
                        if($error == 0 ) {
                             if($size < 2000000){
                                if($format == 'image/jpeg') {
                                   if(file_exists($namafile)){
                                         $namafile = str_replace(".jpg", "", $namafile);
                                         $namafile = $namafile. "_". $time . ".jpg";
                                    }
                
                                     move_uploaded_file($asal, $namafile);
                                        echo 'berhasil upload';
                                     } else {
                                    echo 'formatnya harus jpeg';
                                        }
                                     }else{
                                    echo 'gambarnya kegedean';
                                     }
                                      }else{
                                     echo 'ada error';
                                      }
    
                         move_uploaded_file($asal, 'upload/coach/'. $nama);

                        if(!empty(trim($namec)) && !empty(trim($kota))){
                            if(tambah_coach($namafile, $namec, $sort,  $kota)){
                                header('location: coach.php');
                            }else{
                                $error='ada masalah saat menambah data';
                            }

                        }else{
                          $error = 'judul dan konten wajib diisi';
                   }
                        }

require_once "view/header.php";
?>

        <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            Executive Coach 
                            <small>Tambah Coach</small>
                        </h1>
                        <ol class="breadcrumb">
                            <li>
                                <i class="fa fa-dashboard"></i>  <a href="index.php">Dashboard</a>
                            </li>
                            <li>
                                <i class="fa fa-image"></i>  <a href="galeri.php">Galeri</a>
                            </li>
                            <li class="active">
                                <i class="fa fa-image"></i> Tambah Coach
                            </li>
                        </ol>
                        
                    
<div id="admincontent" class="admoverflow">
        <div class="box clearfix">

                        <form class="form-horizontal" action="" method="post" enctype="multipart/form-data">
                                
                                <div class="form-group">
                                    <label for="Judul" class="col-md-2 control-label">Nama</label>
                                    <div class="col-md-4">
                                        <input class="form-control" type="text" name="namec" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="Tag" class="col-md-2 control-label">Urutan</label>
                                    <div class="col-md-4">
                                        <input type="number" class="form-control" name="sort">
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <label for="Tag" class="col-md-2 control-label">Kota</label>
                                    <div class="col-md-4">
                                        <input class="form-control" type="text" name="kota">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="nama" class="col-md-2 control-label">Upload File</label>
                                    <div class="col-md-4">
                                        <input type="file" name="gambar" required>
                                    </div>
                                </div>

                                
                                
                                <div class="form-group">
                                    <div class="col-md-4 col-md-offset-2">
                                        <input type="submit" name="submit" value="SIMPAN" class="btn btn-success" />
                                    </div>
                </div>
                        </form>
        </div><!--box clearfix-->
    </div><!--admincontent-->

                                   </div>
                    
                </div>
                <!-- /.row -->

            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->            


<?php 

require_once "view/footer.php";
    
?>


