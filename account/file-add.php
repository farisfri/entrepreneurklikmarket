<?php

require_once 'core/init.php';
$super_user = $login = false;
    if(cek_level($_SESSION['user']) == 1){
        $super_user = true;

    }else{
        header('Location: index.php');
    }
       $error ='';
       if(isset($_REQUEST['submit'])){
           $judul = $_POST['judul'];
            $kategori = $_POST['kategori'];
            $sort = $_POST['sort'];
            $user_post = $_SESSION['user'];

           $time = time();
           $nama = $_FILES['file']['name'];
           $error = $_FILES['file']['error'];
           $size = $_FILES['file']['size'];
           $asal = $_FILES['file']['tmp_name'];
           $format = $_FILES['file']['type'];
           $namafile = 'upload/pdf/'. $nama;

        if($error == 0 ) {
          if($size < 556870912){
              if($format == 'application/pdf' || 'image/jpeg') {
                if(file_exists($namafile)){
                      $error='Nama file ada yang sama, Ganti nama file dahulu';
                 } else {
                     if(!empty(trim($judul)) && !empty(trim($namafile))){
                      if(tambah_file($namafile, $judul, $kategori, $sort, $user_post)){
                           move_uploaded_file($asal, 'upload/pdf/'. $nama);
                           move_uploaded_file($asal, $namafile);
                         header('location: file.php');
                      }else{
                          $error='ada masalah saat menambah data';
                      }
                    }else{
                      $error = 'judul dan konten wajib diisi';
                    }
                  }
              } else {
                echo 'formatnya harus pdf';
              }
            }else{
              echo 'Video Size Minimal adalah 64MB';
          }
        }else{
          echo 'ada error';
        }
     }

     

require_once "view/header.php";
?>

        <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            File Upload
                            <small></small>
                        </h1>
                        <ol class="breadcrumb">
                            <li>
                                <i class="fa fa-dashboard"></i>  <a href="index.php">Dashboard</a>
                            </li>
                            <li class="active">
                                <i class="fa fa-book"></i> File Upload
                            </li>
                        </ol>

<form class="form-group" action="" method="post" enctype="multipart/form-data">
    <div class="col-md-12">
        <div class="col-md-6">
             <div class="form-group">
                <label for="Judul">Judul</label>
                <input type="text" class="form-control" name="judul">
             </div>

             <div class="form-group">
                 <select name="kategori" class="form-control">
                         <!--<option value="1">Informasi</option>-->
                         <option value="2">Lifetime</option>
                         <option value="3">Klik Market</option>
                         <option value="4">Klik Raja Voucher</option>
                         <option value="5">Klik Business</option>
                         <option value="6">Entrepreneur Academy</option>
                         <option value="7">Panduan</option>
                         <option value="8">Panduan (No Download)</option>
                 </select>
             </div>
          </div>

          <div class="col-md-6">
            <div class="form-group">
               <label for="Tag">Urutan</label>
               <input type="number" class="form-control" name="sort">
            </div>
          </div>

          <div class="col-md-6">
            <div class="form-group">
                <label for="nama" class="col-md-2 control-label">Upload File</label>
                  <div class="col-md-4">
                    <input type="file" name="file" required>
                  </div>
            </div>
          </div>

         <div id="error"><?= $error ?></div><br>
         <button type="submit" name="submit" class="btn btn-success">Submit</button>
    </div>
</form>

                                   </div>

                </div>
                <!-- /.row -->

            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->


<?php

require_once "view/footer.php";

?>
