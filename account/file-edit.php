
<?php
require_once "core/init.php";
//proteksi
if(!isset($_SESSION['user'])) {
		header('Location: login.php');
}

$super_user = $login = false;
    if(cek_level($_SESSION['user']) == 1){
        $super_user = true;

    }else{
        header('Location: index.php');
    }

$error ='';
$id = $_GET['id'];

if(isset($_GET['id'])){

	$article =tampil_perFile($id);
	while($row=mysqli_fetch_assoc($article)){
		$judul_awal = $row['judul'];
        $kategori_awal = $row['kategori'];
		$sort = $row['sort'];
	}
}

if(isset($_POST['submit'])){
	$judul = $_POST['judul'];
    $kategori = $_POST['kategori'];
	$sort = $_POST['sort'];

	if(!empty(trim($judul)) && !empty(trim($kategori))){
		if(edit_file($judul, $kategori, $sort, $id)){
			header('location: file.php');
		}else{
			$error='ada masalah saat update data';
		}

	}else{
		$error = 'judul dan konten wajib diisi';
	}
}
require_once "view/header.php";
?>

 <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            File Edit
                            <small></small>
                        </h1>
                        <ol class="breadcrumb">
                            <li>
                                <i class="fa fa-dashboard"></i>  <a href="index.php">Dashboard</a>
                            </li>
                            <li class="active">
                                <i class="fa fa-file"></i> File Edit
                            </li>
                        </ol>



<form class="form-group" action="" method="post">
    <div class="col-md-12">
        <div class="col-md-6">
             <div class="form-group">
                <label for="Judul">Judul</label>
                <input type="text" class="form-control" name="judul" value="<?=$judul_awal; ?>">
             </div>

             <div class="form-group">
							 <select name="kategori" class="form-control">
                                             <!--<option value="1" <?php if($kategori_awal == '1'){ echo 'selected'; } ?>>Informasi</option>-->
                                             <option value="2" <?php if($kategori_awal == '2'){ echo 'selected'; } ?>>Lifetime</option>
                                             <option value="3" <?php if($kategori_awal == '3'){ echo 'selected'; } ?>>Klik Market</option>
                                             <option value="4" <?php if($kategori_awal == '4'){ echo 'selected'; } ?>>Klik Raja Voucher</option>
                                             <option value="5" <?php if($kategori_awal == '5'){ echo 'selected'; } ?>>Klik Business</option>
                                             <option value="6" <?php if($kategori_awal == '6'){ echo 'selected'; } ?>>Entrepreneur Academy</option>
                                             <option value="7" <?php if($kategori_awal == '7'){ echo 'selected'; } ?>>Panduan</option>
                                             <option value="8" <?php if($kategori_awal == '8'){ echo 'selected'; } ?>>Panduan (No Download)</option>
							 </select>
             </div>
          </div>
          <div class="col-md-6">

						<div class="form-group">
								<label for="Tag">Sort</label>
							 <input type="text" class="form-control" name="sort" value="<?=$sort; ?>">
						</div>

          </div>
         <div id="error"><?= $error ?></div><br>
         <button type="submit" name="submit" class="btn btn-success">Submit</button>
    </div>
</form>

</div>
</div>
</div>
</div>




<?php

require_once "view/footer.php";

?>
