<?php

require_once 'core/init.php';


 $articles =tampilFile();
//set session true/false
$super_user = $login = false;
    if(cek_level($_SESSION['user']) == 1){
        $super_user = true;

    }else{
        header('Location: index.php');
    }

if (isset($_GET['cari'])) {
    $cari = $_GET['cari'];
    $articles =hasil_cari($_GET['cari']);
}

//Pagination
$perPage = 5000; //perhalaman
$page = isset($_GET["halaman"]) ? (int)$_GET["halaman"] : 1;
$start = ($page > 1) ? ($page * $perPage) - $perPage : 0;
$articles = "SELECT * FROM file LIMIT $start, $perPage";
$result2 = mysqli_query($link, $articles);
$result = mysqli_query($link, "SELECT * FROM file");
$total = mysqli_num_rows($result);
$pages = ceil($total/$perPage);
//end of Pagination

require_once "view/header.php";
?>

        <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">

<!--
<form action"" method="get">
    <input type="search" name="cari" placeholder="Cari">
</form>-->
<?php if($super_user == true): ?>

            <br> &nbsp
            <?php endif; ?>

            <div class="card">
                <div class="header">
                    <h4 class="title">File</h4>
                    <p class="category"></p>
                    <a href="file-add.php"><button class="btn btn-primary tombol">+ Tambah File</button></a>
                </div>
                <div class="content table-responsive">
                    <table class="table table-striped" id="dataFile">
                        <thead>
                            <tr>
                                <th>Opsi</th>
                               <th>No.</th>
                                <th>Judul</th>
                                <th>Waktu</th>
                               <th>Kategori File</th>
                               <th>Urutan</th>
                                
                            </tr>
                        </thead>
                        <tbody>
                            <?php while($row=mysqli_fetch_assoc($result2)):  ?>
                            <tr><td><!--proteksi-->
                                    
                                <?php if($super_user == true): ?>
                                    <a class="btn btn-danger" href="file-delete.php?id=<?= $row['id']; ?>" onclick="return confirm('Anda yakin mau menghapus item ini ?')"> <i class="ti-trash"></i> </a>
                                    <a class="btn btn-primary" href="file-edit.php?id=<?= $row['id']; ?>"> <i class="ti-pencil-alt"></i> </a>
                                   
                                <?php endif; ?></td>
                                 <td><?= $row['id'];?></td>
                                    <td><?= $row['judul']; ?></td>
                                    <td><?= $row['waktu']; ?></td>
                                    <td><?php

                                                if($row['kategori'] == 1){
                                                    echo "Informasi";
                                                }elseif($row['kategori'] == 2){
                                                    echo "Lifetime";
                                                }elseif($row['kategori'] == 3){
                                                    echo "Klik Market";
                                                }elseif($row['kategori'] == 4){
                                                    echo "Klik Raja Voucher";
                                                }elseif($row['kategori'] == 5){
                                                    echo "Klik Business";
                                                }elseif($row['kategori'] == 6){
                                                    echo "Entrepreneur Academy";
                                                }elseif($row['kategori'] == 7){
                                                    echo "Panduan";
                                                }elseif($row['kategori'] == 8){
                                                    echo "Panduan (No Download)";
                                                }
                                                ?></td>
                                    <td><?= $row['sort']; ?></td>
                                    

                                        <?php endwhile; ?>
                            </tr>
                        </tbody>
                    </table>

                </div>
            </div>




<!-- Pagination -->

<?php while ($row = mysqli_fetch_assoc($result2)) { ?>
    <!--<p><?php echo $row["judul"] ?></p>
    <p><?php echo $row["isi"] ?></p>-->

<?php } ?>

<nav aria-label="Page navigation">
  <ul class="pagination">

    <?php for($i=1; $i<=$pages; $i++){ ?>
    <li><a href="?halaman=<?php echo $i?>"><?php echo $i?></a></li>

    </a> <?php } ?>

  </ul>
</nav>


                                   </div>

                </div>
                <!-- /.row -->

            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->


<?php

require_once "view/footer.php";

?>
