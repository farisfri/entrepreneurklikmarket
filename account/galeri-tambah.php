<?php

require_once 'core/init.php';
$super_user = $login = false;
    if(cek_level($_SESSION['user']) == 1){
        $super_user = true;

    }else{
        header('Location: index.php');
    }
                    $error ='';
                    if(isset($_POST['submit'])){
                        $judul = $_POST['judul'];
                        $kategori = $_POST['kategori'];
						$tag = $_POST['tag'];
                        $active = $_POST['active'];

                        $time = time();
                        $nama = $_FILES['gambar']['name'];
                        $error = $_FILES['gambar']['error'];
                        $size = $_FILES['gambar']['size'];
                        $asal = $_FILES['gambar']['tmp_name'];
                        $format = $_FILES['gambar']['type'];
                        $namafile = 'upload/img/'. $nama;

                        if($error == 0 ) {
                            if($size < 2000000){
                                if($format == 'image/jpeg') {
                                   if(file_exists($namafile)){
                                         $namafile = str_replace(".jpg", "", $namafile);
                                         $namafile = $namafile. "_". $time . ".jpg";
                                    }
                                         if(!empty(trim($judul)) && !empty(trim($tag))){
                                             if(tambah_galeri($namafile, $judul, $kategori, $tag, $active)){
                                                move_uploaded_file($asal, $namafile);
                                                move_uploaded_file($asal, 'upload/img/'. $nama);
                                                header('location: galeri.php');
                                             }else{
                                                 $error='ada masalah saat menambah data';
                                             }

                                         }else{
                                           $error = 'judul dan konten wajib diisi';
                                         }
                                     
                                } else {
                                    echo 'formatnya harus jpeg';
                                }
                            }else{
                                echo 'gambarnya kegedean';
                            }
                        }else{
                            echo 'ada error';
                        } 
                    }

require_once "view/header.php";
?>

        <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            Galeri
                            <small>Tambah Foto</small>
                        </h1>
                        <ol class="breadcrumb">
                            <li>
                                <i class="fa fa-dashboard"></i>  <a href="index.php">Dashboard</a>
                            </li>
                            <li>
                                <i class="fa fa-image"></i>  <a href="galeri.php">Galeri</a>
                            </li>
                            <li class="active">
                                <i class="fa fa-image"></i> Tambah Galeri
                            </li>
                        </ol>


<div id="admincontent" class="admoverflow">
        <div class="box clearfix">

                        <form class="form-horizontal" action="" method="post" enctype="multipart/form-data">

                                <div class="form-group">
                                    <label for="Judul" class="col-md-2 control-label">Judul</label>
                                    <div class="col-md-4">
                                        <input class="form-control" type="text" name="judul" required>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="nama" class="col-md-2 control-label">Upload File</label>
                                    <div class="col-md-4">
                                        <input type="file" name="gambar" required>
                                    </div>
                                    <label class="control-label">*Ukuran gambar slider adalah 1440x650</label>
                                </div>



                                <div class="form-group">
                                    <label for="status" class="col-md-2 control-label">Kategori</label>
                                    <div class="col-md-4">
                                        <select name="kategori" class="form-control">
                                                <option value="0">Slider Carousel</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="Tag" class="col-md-2 control-label">Tag</label>
                                    <div class="col-md-4">
                                        <input class="form-control" type="text" name="tag">
                                    </div>
                                </div>
                                <div class="form-group">
	                                  <label class="col-sm-2 control-label">Aktivasi</label>
	                                     <div class="col-sm-10">
											   <div class="checkbox checkbox-inline">
												   <input id="checkbox50" type="hidden" name="active" value="0">
                                                    <input id="checkbox50" type="checkbox" name="active" value="1" >
												   <label for="checkbox50">
													   Aktifkan Slider (Fitur ini hanya untuk salah satu gambar saja)
												   </label>
											   </div>
                                       </div>
                                    </div>

                                <div class="form-group">
                                    <div class="col-md-4 col-md-offset-2">
                                        <input type="submit" name="submit" value="SIMPAN" class="btn btn-success" />
                                    </div>
                </div>
                        </form>
        </div><!--box clearfix-->
    </div><!--admincontent-->

                                   </div>

                </div>
                <!-- /.row -->

            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->


<?php

require_once "view/footer.php";

?>