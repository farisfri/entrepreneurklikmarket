<?php 

require_once 'core/init.php';
$super_user = $login = false;
    if(cek_level($_SESSION['user']) == 1){
        $super_user = true;

    }else{
        header('Location: index.php');
    }
                    $error ='';
                    if(isset($_POST['submit'])){
                        $judul = $_POST['judul'];
                        $konten = $_POST['konten'];
                        $tag = $_POST['tag'];
                        $kategori = $_POST['kategori'];

                        if(!empty(trim($judul)) && !empty(trim($konten))){
                            if(tambah_berita($judul, $konten, $tag, $kategori)){
                                header('location: news.php');
                            }else{
                                $error='ada masalah saat menambah data';
                            }

                        }else{
                          $error = 'judul dan konten wajib diisi';
                   }
                        }

require_once "view/header.php";
?>

        <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                      <div class="card">
                        <div class="header">
                        <h1 class="page-header">
                            Add News
                            <small>Pengumuman</small>
                        </h1>
                        <ol class="breadcrumb">
                            <li>
                                <i class="fa fa-dashboard"></i>  <a href="index.php">Dashboard</a>
                            </li>
                            <li class="active">
                                <i class="fa fa-file"></i> Add News
                            </li>
                        </ol>
                        </div>
                        
                        <div class="content">
                          <form class="form-group" action="" method="post" style="position: relative;">

                                  <div class="col-lg-6">
                                       <div class="form-group">
                                          <label for="Judul">Judul</label>
                                          <input type="text" class="form-control" name="judul">
                                       </div>
                                       <div class="form-group">
                                           <label for="Tag">Tag</label>
                                          <input type="text" class="form-control" name="tag">
                                       </div>
                                       <div class="form-group">
                                           <label for="status" class="col-md-2 control-label">Kategori</label>
                                           <div class="col-md-4">
                                               <select name="kategori" class="form-control">
                                                       <option value="0">Dashboard</option>
                                                       <option value="1">Pop Up</option>
                                               </select>
                                           </div>
                                       </div>
                                    </div>
                                    <div class="col-lg-6">
                                      <div class="form-group">
                                           <label for="Konten">Konten</label>
                                           <textarea name="konten" class="form-control" rows="8" cols="40"></textarea>
                                      </div>
                                    </div>
                                   <div id="error"><?= $error ?></div><br>
                                   <button type="submit" name="submit" class="btn btn-success">Submit</button>

                          </form>
                        </div>

                      </div>
                    </div>
                </div>
                <!-- /.row -->

            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->            


<?php 

require_once "view/footer.php";
    
?>
