<?php 
require_once "core/init.php";
//proteksi
if(!isset($_SESSION['user'])) {
		header('Location: login.php');
}

$super_user = $login = false;
    if(cek_level($_SESSION['user']) == 1){
        $super_user = true;

    }else{
        header('Location: index.php');
    }  

$error ='';

$id = $_GET['id'];

if(isset($_GET['id'])){

	$article =show_per_id($id);
	while($row=mysqli_fetch_assoc($article)){
		$judul_awal = $row['judul'];
		$isi_awal = $row['isi'];
		$tag_awal = $row['tag'];
        $kategori_awal = $row['kategori'];
	}
}

if(isset($_POST['submit'])){
	$judul = $_POST['judul'];
	$konten = $_POST['konten'];
	$tag = $_POST['tag'];
    $kategori = $_POST['kategori'];

	if(!empty(trim($judul)) && !empty(trim($konten))){
		if(edit_berita($judul, $konten, $tag, $kategori, $id)){
			header('location: news.php');
		}else{
			$error='ada masalah saat update data';
		}

	}else{
		$error = 'judul dan konten wajib diisi';
	}
}
require_once "view/header.php";
?>

<div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            Edit News
                            <small></small>
                        </h1>


<form class="form-group" action="" method="post">
    <div class="col-md-12">
        <div class="col-md-6">
             <div class="form-group">
                <label for="Judul">Judul</label>
                <input type="text" class="form-control" name="judul" value="<?=$judul_awal; ?>">
             </div>
             <div class="form-group">
                 <label for="Tag">Tag</label>
                <input type="text" class="form-control" name="tag" value="<?=$tag_awal; ?>">
             </div>
             <div class="form-group">
                 <select name="kategori" class="form-control">
                                 <option value="0" <?php if($kategori_awal == '0'){ echo 'selected'; } ?>>Dashboard</option>
                                 <option value="1" <?php if($kategori_awal == '1'){ echo 'selected'; } ?>>Pop Up</option>
                 </select>
             </div>
          </div>
          <div class="col-md-6">
            <div class="form-group">
                 <label for="Konten">Konten</label>
                 <textarea name="konten" class="form-control" rows="8" cols="40"><?=$isi_awal; ?></textarea>
            </div>
          </div>
         <div id="error"><?= $error ?></div><br>
         <button type="submit" name="submit" class="btn btn-success">Submit</button>
    </div>
</form>

</div>
</div>
</div>
</div>

<?php 

require_once "view/footer.php";
?>
    
