<?php 
require_once "core/init.php";
require_once "view/header.php";
    
$super_user = $login = false;
    if(cek_level($_SESSION['user']) == 1){
        $super_user = true;

    }else{
        header('Location: index.php');
    }
    
$error ='';
$id = $_GET['id'];

if(isset($_GET['id'])){

	$article =show_per_id($id);
	while($row=mysqli_fetch_assoc($article)){
		$judul_awal = $row['judul'];
		$isi_awal = $row['isi'];
		$tag_awal = $row['tag'];
	}
}

?>

 <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            Single Page
                            <small></small>
                        </h1>
                        <ol class="breadcrumb">
                            <li>
                                <i class="fa fa-dashboard"></i>  <a href="index.php">Dashboard</a>
                            </li>
                            <li class="active">
                                <i class="fa fa-file"></i> Single Page
                            </li>
                        </ol>

<p id="judul_single">
<?= $judul_awal; ?>
</p>


<p id="isi_single">
<?= $isi_awal; ?>
</p>

<p id="tag_single">
<?= $tag_awal; ?>
</p>

</div>
</div>
</div>
</div>


<?php 

require_once "view/footer.php";
    
?>