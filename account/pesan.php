<?php 
require_once "core/init.php";

    //proteksi
if(!isset($_SESSION['user'])) {
        header('Location: login.php');
}


require_once "view/header.php";

//Pagination
$perPage = 5000; //perhalaman
$page = isset($_GET["halaman"]) ? (int)$_GET["halaman"] : 1;
$start = ($page > 1) ? ($page * $perPage) - $perPage : 0;
$articles = "SELECT * FROM pertanyaan WHERE username_user = '".$userId."' LIMIT $start, $perPage";
$result2 = mysqli_query($link, $articles);
$result = mysqli_query($link, "SELECT * FROM pertanyaan");
$total = mysqli_num_rows($result);
$pages = ceil($total/$perPage);
//end of Pagination


?>

 <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <br> &nbsp

                        <div class="card">
                            <div class="header">
                                <h4 class="title">Pesan</h4>
                                <p class="category"></p>
                            </div>
                            <div class="content table-responsive">
                                <table class="table table-striped display nowrap" id="dataPesan">
                                    <thead>
                                       <tr>
                                        <th>Opsi</th>
                                           <th>No</th>
                                           <th>Nama</th>
                                           <th>Email</th>
                                           <th>No. HP</th>
                                           <th>Subjek</th>
                                           <th>Pesan</th>
                                           
                                   
                                       </tr>
                                    </thead>
                                        <tbody>
                                            <?php while($row=mysqli_fetch_assoc($result2)):  ?>
                                            <tr>
                                                <td><a class="btn btn-danger" href="pesan-delete.php?id=<?= $row['id']; ?>" onclick="return confirm('Anda yakin mau menghapus item ini ?')"> <i class="ti-trash"></i> </a></td>
                                                <td><?= $row['id'];?></td>
                                                <td><?= $row['nama']; ?></td>
                                                <td><?= $row['email']; ?></td>
                                                <td><?= $row['no_hp']; ?></td>
                                                <td><?= $row['subjek']; ?></td>
                                                <td><?= $row['pesan']; ?></td>
                                                

                                                <?php endwhile; ?>
                                        </tbody>
                                </table>
                            </div>
                        </div>
                     </div>

                </div>
                <!-- /.row -->

            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->

<?php

require_once "view/footer.php"; 
?>

