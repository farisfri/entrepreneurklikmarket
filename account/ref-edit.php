<?php

require_once "core/init.php";
//proteksi
if(!isset($_SESSION['user'])) {
		header('Location: login.php');
}

$super_user = $login = false;
    if(cek_level($_SESSION['user']) == 1){
        $super_user = true;
    }else{
        header('Location: index.php');
    }

$error ='';
$id = $_GET['id'];
if(isset($_GET['id'])){
	$article =tampilkan_per_Referer($id);
	while($row=mysqli_fetch_assoc($article)){
		$nama = $row['nama'];
		$email = $row['email'];
		$no_hp = $row['no_hp'];
	}
}


if(isset($_POST['submit'])){
	$nama = $_POST['nama'];
	$email = $_POST['email'];
	$no_hp = $_POST['no_hp'];

	if(!empty(trim($nama)) && !empty(trim($email))){
		if(edit_Referer( $nama, $email, $no_hp, $id)){
			header('location: referer.php');
		}else{
			$error='ada masalah saat update data';
		}
	}else{
		$error = 'judul dan konten wajib diisi';
	}
}

require_once "view/header.php";

?>



<div id="page-wrapper">



            <div class="container-fluid">



                <!-- Page Heading -->

                <div class="row">

                    <div class="col-lg-12">

                        <h1 class="page-header">

                            Edit Referer

                            <small></small>

                        </h1>

                        <ol class="breadcrumb">

                            <li>

                                <i class="fa fa-dashboard"></i>  <a href="index.php">Dashboard</a>

                            </li>

                            <li class="active">

                                <i class="fa fa-bell"></i> Referer

                            </li>

                        </ol>



<form action="" method="post">

        <div class="form-group">

		<table class="table table-striped table-bordered">

			<tr>

				<td>Nama</td>

				<td>

					<input class="form-control" type="hidden" name="id" value="<?php echo $row['id'] ?>">

					<input class="form-control" type="text" name="nama" value="<?=$nama; ?>">

				</td>

			</tr>

			<tr>

				<td>Email</td>

				<td><input class="form-control" type="text" name="email" value="<?=$email; ?>"></td>

			</tr>

			<tr>

				<td>No. Handphone</td>

				<td><input class="form-control" type="text" name="no_hp" value="<?=$no_hp; ?>"></td>

			</tr>



		</table>

            <div id="error"><?= $error ?></div><br>

            <input class="form-control btn btn-success" name="submit" type="submit" value="Simpan">

            </div>

	</form>





                        </div>



                </div>

                <!-- /.row -->



            </div>

            <!-- /.container-fluid -->



        </div>

        <!-- /#page-wrapper -->





<?php



require_once "view/footer.php";



?>

