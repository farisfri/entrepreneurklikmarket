<?php
require_once "core/init.php";

$super_user = $login = false;
    if(cek_level($_SESSION['user']) == 1){
        $super_user = true;

    }else{
        header('Location: index.php');
    }

require_once "view/header.php";

//Pagination
$perPage = 5000; //perhalaman
$page = isset($_GET["halaman"]) ? (int)$_GET["halaman"] : 1;
$start = ($page > 1) ? ($page * $perPage) - $perPage : 0;
$articles = "SELECT * FROM user WHERE aktivasi='1' LIMIT $start, $perPage";
$result2 = mysqli_query($link, $articles);
$result = mysqli_query($link, "SELECT * FROM user");
$total = mysqli_num_rows($result);
$pages = ceil($total/$perPage);
//end of Pagination

?>

    <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">


                            <br> &nbsp

                            <div class="card">
                                <div class="header">
                                    <h4 class="title">List Referer</h4>
                                    <p class="category"></p>
                                </div>
                                <div class="content table-responsive table-full-width">
                                    <table class="table table-striped table-opt" id="dataUser">
                                        <thead>
                                            <tr>
                                                <th>No</th>
                                                <th>Username</th>
                                                <th>Nama</th>
                                                <th>Referensi dari</th>
                                                <th>Link Referer</th>
                                                <!--<th>Opsi</th>-->
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php while($row=mysqli_fetch_assoc($result2)):  ?>
                                            <tr>
                                                <td><?= $row['id']; ?></td>
                                                <td><?= $row['username']; ?></td>
                                                <td><?= $row['nama']; ?></td>
                                                <td><?= $row['referensi']; ?></td>
                                                <td>https://www.entrepreneurklikmarket.com/?ref=<?= $row['link'];?></td>
                                                <!--<td>
                                                    <a class="btn btn-primary" href="user-edit.php?id=<?= $row['id']; ?>"><i class="ti-pencil-alt"></i> </a>
                                                    <a class="btn btn-warning" href="user-pass.php?id=<?= $row['id']; ?>"><i class="ti-key"></i> </a>
                                                    <a class="btn btn-danger" href="user-delete.php?id=<?= $row['id']; ?>"><i class="ti-trash"></i> </a>
                                                </td>-->

                                                <?php endwhile; ?>
                                            </tr>
                                        </tbody>
                                    </table>

                                </div>
                            </div>
                    </div>
                </div>
            </div>
        </div>

<?php

require_once "view/footer.php";

?>
