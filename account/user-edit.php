<?php

require_once "core/init.php";

//proteksi

if(!isset($_SESSION['user'])) {
		header('Location: login.php');
}

$super_user = $login = false;
    if(cek_level($_SESSION['user']) == 1){
        $super_user = true;
    }else{
        header('Location: index.php');
    }
$error ='';
$id = $_GET['id'];
if(isset($_GET['id'])){
	$article =tampilkan_per_user($id);
	while($row=mysqli_fetch_assoc($article)){
		$username_a = $row['username'];
		$nama_a = $row['nama'];
		$email_a = $row['email'];
		$no_hp_a = $row['no_hp'];
		$kota_a = $row['kota'];
		$kit_type_a = $row['kit_type'];
		$level_a = $row['level'];
		$id_klikMar_a = $row['id_klikMar'];
		$no_ktp_a = $row['no_ktp'];
        $tgl_lahir_a = $row['tgl_lahir'];
        $bank_a = $row['bank'];
        $cabang_bank_a = $row['cabang_bank'];
        $kota_bank_a = $row['kota_bank'];
        $no_rekening_a = $row['no_rekening'];
        $nama_rekening_a = $row['nama_rekening'];
	}
}

if(isset($_POST['submit'])){
	$username = $_POST['username'];
	$nama = $_POST['nama'];
	$email = $_POST['email'];
	$no_hp = $_POST['no_hp'];
	$kota = $_POST['kota'];
	$level = $_POST['level'];
	$id_klikMar = $_POST['id_klikMar'];
	$no_ktp = $_POST['no_ktp'];
    $tgl_lahir = $_POST['tgl_lahir'];
    $bank = $_POST['bank'];
    $cabang_bank = $_POST['cabang_bank'];
    $kota_bank = $_POST['kota_bank'];
    $no_rekening = $_POST['no_rekening'];
    $nama_rekening = $_POST['nama_rekening'];



	if(!empty(trim($username)) && !empty(trim($email))){
		if(edit_user($username, $nama, $email, $no_hp, $kota, $level, $id_klikMar, $no_ktp, $tgl_lahir, $bank, $cabang_bank, $kota_bank, $no_rekening, $nama_rekening, $id)){
			header('location: user.php');
		}else{
			$error='ada masalah saat update data';
		}
	}else{
		$error = 'judul dan konten wajib diisi';
	}

}

require_once "view/header.php";

?>

<div class="content">
    <div class="container-fluid">
       <div class="row">
            <div class="card">
                <div class="header">
                    <div class="container">
                        <h4 class="title">Edit User</h4>
                        <p class="category"></p>
                    </div>
                </div>

                <div class="content">
                    <div class="container">
                        <form class="form-group" action="" method="post" id="inputUser">
                            <div class="col-md-12">
                                <div class="col-md-6">
                                     <div class="form-group">
                                        <label for="username">Username</label>
                                        <input class="form-control" type="hidden" name="id" value="<?php echo $row['id'] ?>">
                                        <input type="text" class="form-control" name="username" value="<?=$username_a; ?>">
                                     </div>
                                     <div class="form-group">
                                        <label for="nama">Nama</label>
                                        <input type="text" class="form-control" name="nama" value="<?= $nama_a; ?>" >
                                     </div>
                                     
                                     <div class="form-group">
                                        <label for="email">Email</label>
                                        <input type="email" class="form-control" name="email" value="<?=$email_a; ?>">
                                     </div>

                                     <div class="form-group">
                                        <label for="no_hp">No. Hp</label>
                                        <input type="number" class="form-control" name="no_hp" value="<?=$no_hp_a; ?>">
                                     </div>
                                     <div class="form-group">
                                        <label for="kota">Kota</label>
                                        <input type="text" class="form-control" name="kota" value="<?= $kota_a; ?>">
                                     </div>
									<div class="form-group">
										<label>Tanggal Lahir <small>(required)</small></label>
										<input type="text" data-custom-class="form-control combo-date" style="display: inline-block;" id="date" data-format="YYYY-MM-DD" data-template="D MMM YYYY" name="tgl_lahir" value="<?= $tgl_lahir_a; ?>">
									</div>
                                     <div class="form-group">
                                         <label for="level">Level</label>
                                         <select class="form-control" name="level">
                                             <option value="1" <?php if($level_a == '1'){ echo 'selected'; } ?>>Administrator</option>
 											<option value="2" <?php if($level_a == '2'){ echo 'selected'; } ?>>User</option>
                                         </select>
                                     </div>

                                  </div>
                                  <div class="col-md-6">
                                  	<div class="form-group">
                                  	   <label for="id_klikMar">ID KlikMarket</label>
                                  	   <input type="number" class="form-control" name="id_klikMar" value="<?=$id_klikMar_a; ?>">
                                  	</div>
                                  	<div class="form-group">
                                  	   <label for="no_ktp">No. KTP</label>
                                  	   <input type="number" class="form-control" name="no_ktp" value="<?=$no_ktp_a; ?>">
                                  	</div>
                                  	<div class="form-group">
                                  	   <label for="bank">Bank</label>
                                  	   <input type="text" class="form-control" name="bank" value="<?= $bank_a; ?>">
                                  	</div>
                                  	<div class="form-group">
                                  	   <label for="cabang_bank">Cabang Bank</label>
                                  	   <input type="text" class="form-control" name="cabang_bank" value="<?= $cabang_bank_a; ?>">
                                  	</div>
                                  	<div class="form-group">
                                  	   <label for="kota_bank">Kota Bank</label>
                                  	   <input type="text" class="form-control" name="kota_bank" value="<?= $kota_bank_a; ?>">
                                  	</div>
                                  	<div class="form-group">
                                  	   <label for="no_rekening">No. Rekening</label>
                                  	   <input type="text" class="form-control" name="no_rekening" value="<?= $no_rekening_a; ?>">
                                  	</div>
                                  	<div class="form-group">
                                  	   <label for="nama_rekening">Nama Pemilik Rekening</label>
                                  	   <input type="text" class="form-control" name="nama_rekening" value="<?= $nama_rekening_a; ?>">
                                  	</div>
                                  </div>
                                 <div id="error"><?= $error ?></div><br>
                                 <a href="user.php" class="btn btn-warning">Kembali</a>
                                 <button type="submit" name="submit" class="btn btn-success">Simpan</button>
                                 <button class="btn btn-danger" type="reset" value="Reset">Reset</button>
                            </div>
                        </form>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
</div>

<?php
require_once "view/footer.php";
?>

