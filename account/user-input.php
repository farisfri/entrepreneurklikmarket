<?php 

require_once "core/init.php";

$super_user = $login = false;
    if(cek_level($_SESSION['user']) == 1){
        $super_user = true;

    }else{
        header('Location: index.php');
    }

if(!isset($_SESSION['user'])) {
		header('Location: index.php');
}else{

$error ='';

if(isset($_POST['submit'])){
    $username = $_POST['username'];
    $pass = $_POST['password'];
    $nama = $_POST['nama'];
    $email = $_POST['email'];
    $no_hp = $_POST['no_hp'];
    $kota = $_POST['kota'];
    $level = $_POST['level'];

    if(!empty(trim($username)) && !empty(trim($pass))){
        
        if(register_cek($username)){

        if(register_userlvl($username, $pass, $nama, $email, $no_hp, $kota, $level)){
            require 'PHPMailer/PHPMailerAutoload.php';
                $mail = new PHPMailer;
                // Konfigurasi SMTP
                $mail->isSMTP();
                $mail->Host = 'mail.entrepreneurklikmarket.com';
                $mail->SMTPAuth = true;
                $mail->Username = 'referer@entrepreneurklikmarket.com';
                $mail->Password = '1hwvyc28xmmy';
                $mail->SMTPSecure = 'ssl';
                $mail->Port = 465;

                $mail->setFrom('referer@entrepreneurklikmarket.com', 'Referensi Entrepreneur Klikmarket');
                $mail->addReplyTo('referer@entrepreneurklikmarket.com', 'Referensi Entrepreneur Klikmarket');

                // Menambahkan penerima
                $mail->addAddress($emailcook);

                // Menambahkan beberapa penerima
                //$mail->addAddress('penerima2@contoh.com');
                //$mail->addAddress('penerima3@contoh.com');

                // Menambahkan cc atau bcc
                $mail->addCC('');
                $mail->addBCC('');

                // Subjek email
                $mail->Subject = "Halo $name_refer ! Ada pendaftar baru di entrepreneurklikmarket.com";

                // Mengatur format email ke HTML
                $mail->isHTML(true);

                // Konten/isi email
                $mailContent = "<h1>Ada pendaftar baru di entrepreneurklikmarket.com dengan menggunakan ID referensi anda!</h1><br>
                    <p>Harap cek akun anda <br>Berikut data pendaftar referer anda :<br><br> Username : $username <br><br></p>";
                $mail->Body = $mailContent;

                // Menambahakn lampiran
                $mail->addAttachment('');
                $mail->addAttachment(''); //atur nama baru

                // Kirim email
                if(!$mail->send()){
                    echo 'Pesan tidak dapat dikirim.';
                    echo 'Mailer Error: ' . $mail->ErrorInfo;
                }else{
                    echo 'Pesan telah terkirim';
                }
                header('Location: user.php');
            }else{
            $error='ada masalah saat registrasi';
        }
    }else{
            $error='Username sudah ada, pilih username lain';
        }

    }else{
        $error = 'Username dan password wajib diisi';
    }
}
require_once "view/header.php";

?>         
<div class="content">
    <div class="container-fluid">
       <div class="row">
            <div class="card">
                <div class="header">
                    <div class="container">
                        <h4 class="title">Buat User Baru</h4>
                        <p class="category"></p>
                    </div>
                </div>

                <div class="content">
                    <div class="container">
                        <form class="form-group" action="" method="post" id="inputUser">
                            <div class="col-md-12">
                                <div class="col-md-6">
                                     <div class="form-group">
                                        <label for="username">Username</label>
                                        <input type="text" class="form-control" name="username">
                                     </div>
                                     <div class="form-group">
                                         <label for="password">Password</label>
                                        <input type="text" class="form-control" name="password" id="password">
                                     </div>
                                      <div class="form-group">
                                         <label for="password">Ulangi Password</label>
                                        <input type="text" class="form-control" name="cfmPassword" id="cfmPassword">
                                     </div>
                                     <div class="form-group">
                                        <label for="nama">Nama</label>
                                        <input type="text" class="form-control" name="nama">
                                     </div>
                                     


                                  </div>
                                  <div class="col-md-6">
                                    <div class="form-group">
                                       <label for="email">Email</label>
                                       <input type="email" class="form-control" name="email">
                                    </div>
                                    <div class="form-group">
                                       <label for="no_hp">No. Hp</label>
                                       <input type="number" class="form-control" name="no_hp">
                                    </div>
                                    <div class="form-group">
                                       <label for="kota">Kota</label>
                                       <input type="text" class="form-control" name="kota">
                                    </div>
                                    <div class="form-group">
                                        <label for="level">Level</label>
                                        <select class="form-control" name="level">
                                            <option value="1">Administrator</option>
                                            <option value="2">User</option>
                                        </select>
                                    </div>
                                  </div>
                                 <div id="error"><?= $error ?></div><br>
                                 <a href="user.php" class="btn btn-warning">Kembali</a>
                                 <button type="submit" name="submit" class="btn btn-success">Simpan</button>
                                 <button class="btn btn-danger" type="reset" value="Reset">Reset</button>
                            </div>
                        </form>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
</div>


<?php require_once "view/footer.php"; }?>
