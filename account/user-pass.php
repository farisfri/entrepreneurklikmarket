<?php

require_once "core/init.php";

//proteksi

if(!isset($_SESSION['user'])) {

		header('Location: login.php');

}



$super_user = $login = false;

    if(cek_level($_SESSION['user']) == 1){

        $super_user = true;



    }else{

        header('Location: index.php');

    }



$error ='';

$id = $_GET['id'];
if(isset($_GET['id'])){

	$article =tampilkan_per_user($id);

	while($row=mysqli_fetch_assoc($article)){

		$username = $row['username'];

	}

}



if(isset($_POST['submit'])){

	$pass = $_POST['password'];



	if(!empty(trim($username))){

		if(edit_userPass($pass, $id)){

			header('location: user.php');

		}else{

			$error='ada masalah saat update data';

		}



	}else{

		$error = 'judul dan konten wajib diisi';

	}

}

require_once "view/header.php";

?>

<div class="content">
    <div class="container-fluid">
       <div class="row">
            <div class="card">
                <div class="header">
                    <div class="container">
                        <h4 class="title">Edit Password</h4>
                        <p class="category"></p>
                    </div>
                </div>

                <div class="content">
                    <div class="container">
                        <form class="form-group" action="" method="post" id="inputUser">
                            <div class="col-md-12">
                                     <div class="form-group">
                                        <label for="username">Username</label>
                                        <input class="form-control" type="hidden" name="id" value="<?php echo $row['id'] ?>">
                                        <input type="text" class="form-control" name="username" value="<?=$username; ?>" disabled>
                                     </div>
                                     <div class="form-group">
                                         <label for="password">Password</label>
                                        <input type="text" class="form-control" name="password" id="password">
                                     </div>
                                      <div class="form-group">
                                         <label for="password">Ulangi Password</label>
                                        <input type="text" class="form-control" name="cfmPassword" id="cfmPassword">
                                     </div>
                                 <div id="error"><?= $error ?></div><br>
                                 <a href="user.php" class="btn btn-warning">Kembali</a>
                                 <button type="submit" name="submit" class="btn btn-success">Simpan</button>
                                 <button class="btn btn-danger" type="reset" value="Reset">Reset</button>
                            </div>
                        </form>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
</div>

<?php
require_once "view/footer.php";
?>

