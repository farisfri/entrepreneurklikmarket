<?php 

require_once 'core/init.php'; 


    $id = $_GET['id'];
    if(isset($_GET['id'])){
        $article =tampilkan_per_user($id);
        while($row=mysqli_fetch_assoc($article)){
            $username = $row['username'];
            $nama = $row['nama'];
            $email = $row['email'];
            $no_hp = $row['no_hp'];
            $kota = $row['kota'];
            $kit_type = $row['kit_type'];
            $level = $row['level'];
            $id_klikMar = $row['id_klikMar'];
            $no_ktp = $row['no_ktp'];
            $tgl_lahir = $row['tgl_lahir'];
            $bank = $row['bank'];
            $cabang_bank = $row['cabang_bank'];
            $kota_bank = $row['kota_bank'];
            $no_rekening = $row['no_rekening'];
            $nama_rekening = $row['nama_rekening'];
        }
    }

 require_once "view/header.php";           

?>


            <div id="page-wrapper">
                    <div class="container-fluid">
                        <!-- Page Heading -->
                        <div class="row">
                            <div class="col-lg-12">
                                    <div class="card">
                                        <div class="header">
                                            <h4 class="title"><strong>View User</strong></h4>
                                            <p class="category"></p>
                                        </div>
                                        <div class="content" >
                                            <div class="row" style="padding: 30px;">

                                                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                    <h5><strong>Data Akun</strong></h5>
                                                    <table width="100%" border="0">
                                                        <tr>
                                                            <td width="30%">Username</td>
                                                            <td width="5%">:</td>
                                                            <td width="65%"><?= $username ?></td>
                                                        </tr>
                                                        <tr>
                                                            <td width="30%">Kit Type</td>
                                                            <td width="5%">:</td>
                                                            <td width="65%"><?= $kit_type ?></td>
                                                        </tr>
                                                        <tr>
                                                            <td width="30%">ID Klik Market</td>
                                                            <td width="5%">:</td>
                                                            <td width="65%"><?= $id_klikMar ?></td>
                                                        </tr>
                                                    </table>

                                                    <h5 style="margin-top: 50px;"><strong>Data Bank</strong></h5>
                                                    <table width="100%" border="0">
                                                        <tr>
                                                            <td width="30%">Bank</td>
                                                            <td width="5%">:</td>
                                                            <td width="65%" class="uppercase"><?= $bank ?></td>
                                                        </tr>
                                                        <tr>
                                                            <td width="30%">Cabang Bank</td>
                                                            <td width="5%">:</td>
                                                            <td width="65%"><?= $cabang_bank ?></td>
                                                        </tr>
                                                        <tr>
                                                            <td width="30%">Kota Bank</td>
                                                            <td width="5%">:</td>
                                                            <td width="65%"><?= $kota_bank ?></td>
                                                        </tr>
                                                        <tr>
                                                            <td width="30%">No Rekening</td>
                                                            <td width="5%">:</td>
                                                            <td width="65%"><?= $no_rekening ?></td>
                                                        </tr>
                                                        <tr>
                                                            <td width="30%">Nama Rekening</td>
                                                            <td width="5%">:</td>
                                                            <td width="65%"><?= $nama_rekening ?></td>
                                                        </tr>
                                                    </table>
                                                 </div>

                                                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                    <h5><strong>Data Pribadi</strong></h5>
                                                    <table width="100%" border="0">
                                                        <tr>
                                                            <td width="30%">No KTP</td>
                                                            <td width="5%">:</td>
                                                            <td width="65%"><?= $no_ktp ?></td>
                                                        </tr>
                                                         <tr>
                                                            <td width="30%">Kota</td>
                                                            <td width="5%">:</td>
                                                            <td width="65%"><?= $kota ?></td>
                                                        </tr>
                                                        <tr>
                                                            <td width="30%">Nama Lengkap</td>
                                                            <td width="5%">:</td>
                                                            <td width="65%"><?= $nama ?></td>
                                                        </tr>
                                                        <tr>
                                                            <td width="30%">Tanggal Lahir</td>
                                                            <td width="5%">:</td>
                                                            <td width="65%"><?= $tgl_lahir ?></td>
                                                        </tr>
                                                        <tr>
                                                            <td width="30%">Email</td>
                                                            <td width="5%">:</td>
                                                            <td width="65%"><?= $email ?></td>
                                                        </tr>
                                                        <tr>
                                                            <td width="30%">No. HP</td>
                                                            <td width="5%">:</td>
                                                            <td width="65%"><?= $no_hp ?></td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                            </div>
                        </div>
                    </div>
                </div>


<?php 
 require_once "view/footer.php";
?>