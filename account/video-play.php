<?php 

require_once 'core/init.php';
    //proteksi

    if(!isset($_SESSION['user'])) {
        header('Location: login.php');
    }

require_once "view/header.php";


$error ='';
$videoke = $_GET['videoke'];

    if(isset($_GET['videoke'])){
    	$article =tampilkan_per_video($videoke);
    	while($row=mysqli_fetch_assoc($article)){
            $judul = $row['judul'];
            $nama_file = $row['nama_file'];
            $poin_per_video = $row['poin_per_video'];
    	}
        if($videoke == 0){
            if($progvideoku == 0){
               tambahpoin($poin_per_video, $id);
            }
        }
    }

    $terendah = terendah();
    while($row=mysqli_fetch_assoc($terendah)){
        $rendah = $row['videoke'];
    }

    $tertinggi= tertinggi();
    while($row=mysqli_fetch_assoc($tertinggi)){
        $tinggi = $row['videoke'];
    }

    $next=($videoke+1);
    $previous=($videoke-1);

?>
        <div id="page-wrapper">
            <div class="container-fluid">
                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card card-vid" style="width: 100%;">
                            <div class="header">
                                <h4 class="title"><?= $judul ?></h4>
                            </div>
                            <?php 
                                if($videoke==$progvideoku){ ?>
                                    <div class="content embed-responsive embed-responsive-16by9" style="width:100%; height:500px;">
                                        <video class="embed-responsive-item mejs__player" style="width: 100%; height:500px;" controls="controls" preload="none" preload="none" controls playsinline webkit-playsinline>
                                            <source type="video/mp4" src="<?= $nama_file ?>" />
                                            <source type="video/webm" src="<?= $nama_file ?>" />
                                            <source type="video/ogg" src="<?= $nama_file ?>" />
                                            <object width="100%" height="500px" type="application/x-shockwave-flash" data="/player/build/mediaelement-flash-video.swf">
                                                <param name="movie" value="/player/build/mediaelement-flash-video.swf" />
                                                <param name="flashvars" value="controls=true&amp;poster=#&amp;file=<?= $nama_file ?>" />
                                                <img src="#" width="100%" height="500px" title="No video playback capabilities" />
                                            </object>
                                        </video>
                                        
                                    </div>  
                                    <div class="clearfix" style="margin: 30px;">
                                        <a href="video-play.php?id=<?= $id ?>&videoke=<?= $previous ?>&progvide=<?= $progvideoku ?>" class="btn btn-warning" style="float: left; <?php if($videoke == $rendah){ ?> display: none; <?php  } ?>" >Sebelumnya</a>
     
                                             <a href="vide-decision.php?id=<?= $id ?>&videoke=<?= $videoke ?>&progvide=<?= $progvideoku ?>" class="btn btn-success" style="float: right; <?php if($videoke == $tinggi){ ?> display: none; <?php  } ?>">Next</a>

                                    </div>
                                <?php }elseif($videoke>$progvideoku){ ?>
                                    <div class="content">
                                        <h3 class="text-center">Tidak Dapat Melihat Video, Harap Selesaikan Melihat Video Yang Tersedia Terlebih Dahulu</h3>
                                    </div>
                                    <div class="clearfix" style="margin: 30px;">
                                        <a href="video-play.php?id=<?= $id ?>&videoke=<?= $previous ?>&progvide=<?= $progvideoku ?>" class="btn btn-warning" style="float: left; <?php if($videoke == $rendah){ ?> display: none; <?php  } ?>">Sebelumnya</a>
                                        <a href="video-play.php?id=<?= $id ?>&videoke=<?= $progvideoku ?>&progvide=<?= $progvideoku ?>" class="btn btn-danger" style="float: left;">Kembali Ke Video Anda</a>
                                        
                                        <a href="" class="btn btn-success" style="float: right; <?php if($videoke == $tinggi){ ?> display: none; <?php  } ?>" disabled>Selanjutnya</a>
                                    </div>
                                <?php }elseif($videoke<$progvideoku){ ?>
                                    <div class="content embed-responsive embed-responsive-16by9" style="width:100%; height:500px;">
                                        <video class="embed-responsive-item mejs__player" style="width: 100%; height:500px;" controls="controls" preload="none" preload="none" controls playsinline webkit-playsinline>
                                            <source type="video/mp4" src="<?= $nama_file ?>" />
                                            <source type="video/webm" src="<?= $nama_file ?>" />
                                            <source type="video/ogg" src="<?= $nama_file ?>" />
                                            <object width="100%" height="500px" type="application/x-shockwave-flash" data="/player/build/mediaelement-flash-video.swf">
                                                <param name="movie" value="/player/build/mediaelement-flash-video.swf" />
                                                <param name="flashvars" value="controls=true&amp;poster=#&amp;file=<?= $nama_file ?>" />
                                                <img src="#" width="100%" height="500px" title="No video playback capabilities" />
                                            </object>
                                        </video>
                                        
                                    </div>  
                                    <div class="clearfix" style="margin: 30px;">
                                        <a href="video-play.php?id=<?= $id ?>&videoke=<?= $previous ?>&progvide=<?= $progvideoku ?>" class="btn btn-warning" style="float: left; <?php if($videoke == $rendah){ ?> display: none; <?php  } ?>" >Sebelumnya</a>
                                        <a href="video-play.php?id=<?= $id ?>&videoke=<?= $progvideoku ?>&progvide=<?= $progvideoku ?>" class="btn btn-danger" style="float: left;">Kembali Ke Video Anda</a>
                                        <a href="video-play.php?id=<?= $id ?>&videoke=<?= $next ?>&progvide=<?= $progvideoku ?>" class="btn btn-success" style="float: right; <?php if($videoke == $tinggi){ ?> display: none; <?php  } ?>">Next</a>
                                    </div>
                                <?php } ?>

                        </div>
                    </div>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /.page-wrapper -->
<?php 
require_once "view/footer.php";
?>
