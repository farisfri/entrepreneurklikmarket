
    <footer class="footer">
            <div class="container-fluid">
                <nav class="pull-left">
                    <ul>
                        <!--
                        <li>
                            <a href="https://www.creative-tim.com">
                                Creative Tim
                            </a>
                        </li>
                        <li>
                            <a href="https://blog.creative-tim.com">
                               Blog
                            </a>
                        </li>
                        <li>
                            <a href="https://www.creative-tim.com/license">
                                Licenses
                            </a>
                        </li>-->
                    </ul>
                </nav>
                <div class="copyright pull-right">
                    &copy; <script>document.write(new Date().getFullYear())</script>, copyright by <a href="https://www.entrepreneurklikmarket.com">Entrepreneur Klikmarket</a>
                </div>
            </div>
        </footer>

    </div>
</div>

</body>

    <!--   Core JS Files   -->
    <script src="view/js/jquery-1.10.2.js" type="text/javascript"></script>

    
	<script src="view/js/bootstrap.min.js" type="text/javascript"></script>

	<!--  Checkbox, Radio & Switch Plugins -->
	<script src="view/js/bootstrap-checkbox-radio.js"></script>

    <script type="text/javascript" src="view/js/plugins/DataTables/datatables.min.js"></script>
    <script type="text/javascript" src="view/js/plugins/validate/jquery.validate.min.js"></script>
    <script type="text/javascript" src="view/js/plugins/combodate/bootstrap-datepicker.js"></script>
    <script type="text/javascript" src='https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.13.0/moment.min.js'></script>
    <script type="text/javascript" src="view/js/plugins/combodate/combodate.js"></script>
     <script type="text/javascript">

     $(document).ready(function() {
         $('#example').DataTable( {
             "scrollX": true
         } );
     } );

     $(document).ready(function() {
         $('#dataUserVal').DataTable( {
             "pagingType": "full_numbers",
              "scrollX": true
         });
     });
     $(document).ready(function() {
         $('#dataUser').DataTable( {
             "pagingType": "full_numbers",
               "scrollX": true // enable responsive
         });
     });
     $(document).ready(function() {
          $('#dataVide').DataTable( {
             "pagingType": "full_numbers",
              "scrollX": true
         });
     });
     $(document).ready(function() {
           $('#dataFile').DataTable( {
             "pagingType": "full_numbers",
               "scrollX": true
         });
     });
     $(document).ready(function() {
           $('#dataPesan').DataTable( {
             "pagingType": "full_numbers",
             "scrollX": true
         });
     });


     $(document).ready(function() {
         $("#inputUser").validate({
             rules: {
                     "password": {
                         minlength: 6
                     },
                     "cfmPassword": {
                         minlength: 6,
                         equalTo : "#password"
                     }
                 },
                   messages: {
                         "password": 'Harap mengisi password akun, minimal 6 karakter',
                         "cfmPassword": 'Password Tidak Cocok, Pastikan Password Sudah Benar'
                 },
             submitHandler: function (form) { 
                 hashPassword();
                 form.submit();
                 // alert('valid form'); // for demo
                 // return false; // for demo
             }
         });
     });

     $(function(){
        $('#date').combodate({
            minYear: 1930,
            maxYear: 2015
        });
    });
    
     </script>

	<!--  Charts Plugin -->
	<script src="view/js/chartist.min.js"></script>

    <!--  Notifications Plugin    -->
    <script src="view/js/bootstrap-notify.js"></script>

    <!--  Google Maps Plugin    
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js"></script>-->

    <!-- Paper Dashboard Core javascript and methods for Demo purpose -->
	<script src="view/js/paper-dashboard.js"></script>

   <script src="player/build/mediaelement-and-player.min.js"></script>

   <!--<script src="https://vjs.zencdn.net/ie8/1.1.2/videojs-ie8.min.js"></script>
   <script src="https://vjs.zencdn.net/6.9.0/video.js"></script>-->

	

</html>
