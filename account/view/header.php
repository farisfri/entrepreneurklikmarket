<?php

require_once "core/init.php";


$super_user = $login = false;
if(isset($_SESSION['user'])) {
    if(cek_isiData($_SESSION['user']) == 1){
        if(cek_aktivasi($_SESSION['user']) == 1){
            $login = true;
            cek_tgl();
            if(cek_level($_SESSION['user']) == 1){
                $super_user = true;
               }  
            }else{
                unset($_SESSION['user']);
                session_destroy();
                header('location: ../verify.php');
            }
    }else{ //cek isi data
        header('location: ../complete/index.php');
    }
}else{ //isset
    header('location: ../index.php');
}

date_default_timezone_set('Asia/Jakarta');
setlocale(LC_TIME, 'id_ID');
$sekarang = new DateTime();

$date_start = date("Y-m-d");
$date_end = date('Y-m-d', strtotime($date_start. ' + 30 days'));

$usid = $_SESSION['user'];
    if(isset($_SESSION['user'])){
      $article =tampilUsername($usid);
      
      while($row=mysqli_fetch_assoc($article)){
        $id = $row['id'];
        $userId = $row['username'];
        $namaId = $row['nama'];
        $userLink = $row['link'];
        $pointku = $row['poin'];
        $progvideoku = $row['videoke'];
        $tglstartpoin = $row['poin_startdate'];
        $tglendpoin = $row['poin_enddate'];
      }
    }

?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <link rel="apple-touch-icon" sizes="76x76" href="assets/img/apple-icon.png">
    <link rel="icon" type="image/png" sizes="96x96" href="assets/img/favicon.png">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

    <title>Account - Entrepreneur Academy</title>

    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />

    <!-- Bootstrap core CSS     -->

    <link href="view/css/bootstrap.min.css" rel="stylesheet" />
    
     <!-- Compiled and minified CSS -->

    <!-- Animation library for notifications   -->
    <link href="view/css/animate.min.css" rel="stylesheet"/>

    <!--  Paper Dashboard core CSS    -->

    <link href="view/css/paper-dashboard.css" rel="stylesheet"/>
    <link rel="stylesheet" type="text/css" href="view/js/plugins/DataTables/datatables.min.css"/>

    <!--  Fonts and icons     -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
    <link href='https://fonts.googleapis.com/css?family=Muli:400,300' rel='stylesheet' type='text/css'>
    <link href="view/css/themify-icons.css" rel="stylesheet">
    
    <!-- Add any other renderers you need; see Use Renderers for more information -->
    <link rel="stylesheet" href="player/build/mediaelementplayer.min.css" />


    <style>
    .mejs__overlay-button {
        background-image: url("player/build/mejs-controls.svg");
    }
    .mejs__overlay-loading-bg-img {
        background-image: url("player/build/mejs-controls.svg");
    }
    .mejs__button > button {
        background-image: url("player/build/mejs-controls.svg");
    }

    
        #player2-container .mejs__time-buffering, #player2-container .mejs__time-current, #player2-container .mejs__time-handle,
        #player2-container .mejs__time-loaded, #player2-container .mejs__time-hovered, #player2-container .mejs__time-marker, #player2-container .mejs__time-total {
            height: 2px;
        }

        #player2-container .mejs__time-total {
            margin-top: 9px;
        }
        #player2-container .mejs__time-handle {
            left: -5px;
            width: 12px;
            height: 12px;
            border-radius: 50%;
            background: #ffffff;
            top: -5px;
            cursor: pointer;
            display: block;
            position: absolute;
            z-index: 2;
            border: none;
        }
        #player2-container .mejs__time-handle-content {
            top: 0;
            left: 0;
            width: 12px;
            height: 12px;
        }


    </style>
    <!--<link href="https://vjs.zencdn.net/6.9.0/video-js.css" rel="stylesheet">-->

</head>
<body>

<div class="wrapper">
    <div class="sidebar" data-background-color="white" data-active-color="danger">

    <!--
        Tip 1: you can change the color of the sidebar's background using: data-background-color="white | black"
        Tip 2: you can change the color of the active button using the data-active-color="primary | info | success | warning | danger"
    -->

        <div class="sidebar-wrapper">
            <div class="logo">
                <a href="../index.php?ref=<?= $userLink ?>" class="simple-text">
                    <img src="view/img/logoen.png" class="img-fluid" width="60%">
                </a>
            </div>

            <ul class="nav">
                <li>
                    <a href="index.php">
                        <i class="ti-panel"></i>
                        <p>Dashboard</p>
                    </a>
                </li>
                <?php if($super_user == true): ?>
                <li>
                    <a href="user.php">
                        <i class="ti-user"></i>
                        <p>User Profile</p>
                    </a>
                </li>
                <li>
                    <a href="coach.php">
                        <i class="ti-medall"></i>
                        <p>Coach</p>
                    </a>
                </li>
                <li>
                    <a href="blog.php">
                        <i class="ti-video-clapper"></i>
                        <p>Video</p>
                    </a>
                </li>
                <li>
                    <a href="galeri.php">
                        <i class="ti-gallery"></i>
                        <p>Gambar</p>
                    </a>
                </li>
                <li>
                    <a href="file.php">
                        <i class="ti-files"></i>
                        <p>File</p>
                    </a>
                </li>
               
                <li>
                    <a href="news.php">
                        <i class="ti-announcement"></i>
                        <p>Pengumuman</p>
                    </a>
                </li>
                
                <li>
                    <a href="referer.php">
                        <i class="ti-view-list"></i>
                        <p>List Referer</p>
                    </a>
                </li>

                <li>
                    <a href="packet.php">
                        <i class="ti-flag"></i>
                        <p>Outbond</p>
                    </a>
                </li>

                
                <?php endif; ?>
                <li>
                    <a href="referensi.php">
                        <i class="ti-bell"></i>
                        <p>Referensi</p>
                    </a>
                </li>
                <li>
                    <a href="pesan.php">
                        <i class="ti-email"></i>
                        <p>Pesan</p>
                    </a>
                </li>
            </ul>
        </div>
    </div>

    <div class="main-panel">
        <nav class="navbar navbar-default">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar bar1"></span>
                        <span class="icon-bar bar2"></span>
                        <span class="icon-bar bar3"></span>
                    </button>
                    <a class="navbar-brand" href="#">Dashboard</a>
                </div>
                 <div class="collapse navbar-collapse">
                    <ul class="nav navbar-nav navbar-right">
                        <!-- <li class="dropdown">
                              <a href="lifetime.php" class="dropdown-toggle" data-toggle="dropdown">
                                    <i class="ti-shopping-cart"></i>

                                    <p>LIFETIME</p>
                                    <b class="caret"></b>
                              </a>
                              <ul class="dropdown-menu">
                                <li><a href="lifetime.php">LIFETIME</a></li>
                                <li><a href="klik.php">KLIK MARKET</a></li>
                                <li><a href="raja.php">KLIK RAJA VOUCHER</a></li>
                                <li><a href="business.php">KLIK BUSINESS</a></li>
                                <li><a href="ea.php">ENTREPRENEUR ACADEMY</a></li>
                              </ul>
                        </li> -->
                        <li>
                            <a href="https://itslifetime.com/login" style="background-color: orange; padding: 10px 15px 10px 15px;  border-radius: 5px; color: #ffffff; margin-right: 15px;">
                                <i class="ti-book"></i>
                                <p>LOGIN LIFETIME</p>
                            </a>
                        </li>
                        <li>
                            <a href="http://www.lifetimeshare.com/member/index.php?r=site%2Flogin" style="background-color: orange; padding: 10px 15px 10px 15px;  border-radius: 5px; color: #ffffff;">
                                <i class="ti-book"></i>
                                <p>LOGIN LIFETIME SHARE</p>
                            </a>
                        </li>
                        <li>
                            <a href="panduan.php">
                                <i class="ti-book"></i>
                                <p>PANDUAN</p>
                            </a>
                        </li>
                        <li class="dropdown">
                              <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    <i class="ti-user"></i>

                                    <p>Halo, <?php echo $namaId; ?></p>
                                    <b class="caret"></b>
                              </a>
                              <ul class="dropdown-menu">
                                        <?php if($super_user == false): ?>

                                        <li><a href="user-setting.php?id=<?= $id; ?>">Setting</a></li>
                                        <?php endif; ?>
                                        <li><a href="logout.php">Logout</a></li>

                              </ul>
                        </li>
                    </ul>

                </div>
            </div>
        </nav>

