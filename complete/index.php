<?php

require_once "core/init.php";

$login = false;

if(isset($_SESSION['user'])) {
    $login = true;
    $usid = $_SESSION['user'];
    $usRef = tampilUsername($usid);
    while($row=mysqli_fetch_assoc($usRef)){
        $userIdRef = $row['id'];
        $usernameIdRef = $row['username'];
        $emailIdRef = $row['email'];
        $namaIdref = $row['nama'];

    }  
}


if(isset($_GET['ref'])){
    $reference =tampilRef($_GET['ref']);
    while($row=mysqli_fetch_assoc($reference)){
        $idRef = $row['id'];
        $usernameRef = $row['username'];
        $emailRef = $row['email'];
        setcookie("refe", $emailRef, time()+24*60*60);
    }
}

if(isset($_COOKIE['refe'])){
    $emailcook = $_COOKIE['refe'];
}else{
    $emailcook= "daniel.diamond89@gmail.com";
}

if(isset($_POST['submit'])){
	$id_klikMar = $_POST['id_klikMar'];
	$no_ktp = $_POST['no_ktp'];
	$tgl_lahir = $_POST['tgl_lahir'];
    $bank = $_POST['bank'];
	$cabang_bank = $_POST['cabang_bank'];
	$kota_bank = $_POST['kota_bank'];
    $no_rekening = $_POST['no_rekening'];
	$nama_rekening = $_POST['nama_rekening'];

	if(!empty(trim($id_klikMar)) && !empty(trim($no_ktp))){
		if(allValidate($id_klikMar, $no_ktp, $tgl_lahir, $bank, $cabang_bank, $kota_bank, $no_rekening, $nama_rekening, $userIdRef)){
			header('location: ../index.php');
		}else{
			$error='ada masalah saat update data';
		}

	}else{
		$error = 'judul dan konten wajib diisi';
	}
}
?>

<style type="text/css">
.combodate {display:block}
.combodate .form-control {display:inline-block}
</style>

<!doctype html>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	<link rel="apple-touch-icon" sizes="76x76" href="assets/img/apple-icon.png" />
	<link rel="icon" type="image/png" href="../view/assets/img/80x80.png" />
	<title>Complete your data</title>

	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />

	<!-- CSS Files -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet" />
	<link href="assets/css/paper-bootstrap-wizard.css" rel="stylesheet" />

	<!-- CSS Just for demo purpose, don't include it in your project -->
	<link href="assets/css/demo.css" rel="stylesheet" />

	<!-- Fonts and Icons -->
    <link href="http://netdna.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.css" rel="stylesheet">
	<link href='https://fonts.googleapis.com/css?family=Muli:400,300' rel='stylesheet' type='text/css'>
	<link href="assets/css/themify-icons.css" rel="stylesheet">
</head>

<body>
	<div class="image-container set-full-height" style="background-image: url('assets/img/paper-1.jpeg')">
	    <!--   Creative Tim Branding   -->
	    <a href="../">
	         <div class="logo-container">
	            <!--<div class="logo">
	                <img src="../view/assets/img/logoen.png">
	            </div>-->
	            <div class="brand">
	                Entrepreneur Klik Market
	            </div>
	        </div>
	    </a>


	    <!--   Big container   -->
	    <div class="container">
	        <div class="row">
		        <div class="col-sm-8 col-sm-offset-2">

		            <!--      Wizard container        -->
		            <div class="wizard-container">

		                <div class="card wizard-card" data-color="orange" id="wizardProfile">
		                    <form action="" method="post">
		                <!--        You can switch " data-color="orange" "  with one of the next bright colors: "blue", "green", "orange", "red", "azure"          -->

		                    	<div class="wizard-header text-center">
		                        	<h3 class="wizard-title">Perlengkapan Data</h3>
									<p class="category"></p>
		                    	</div>

								<div class="wizard-navigation">
									<div class="progress-with-circle">
									     <div class="progress-bar" role="progressbar" aria-valuenow="1" aria-valuemin="1" aria-valuemax="3" style="width: 21%;"></div>
									</div>
									<ul>
			                            <li>
											<a href="#about" data-toggle="tab">
												<div class="icon-circle">
													<i class="ti-user"></i>
												</div>
												Data Pribadi
											</a>
										</li>
			                            <li>
											<a href="#account" data-toggle="tab">
												<div class="icon-circle">
													<i class="ti-settings"></i>
												</div>
												Informasi Bank
											</a>
										</li>
			                            <li>
											<a href="#address" data-toggle="tab">
												<div class="icon-circle">
													<i class="ti-map"></i>
												</div>
												Konfirmasi
											</a>
										</li>
			                        </ul>
								</div>
		                        <div class="tab-content">
		                            <div class="tab-pane" id="about">
		                            	<div class="row">
		                            		<div class="col-sm-12">
											<h5 class="info-text">1. Data Pribadi</h5>
											<div class="col-sm-5 col-sm-offset-1">
												<div class="form-group">
													<label>ID Klik Market <small>(required)</small></label>
													<input name="id_klikMar" type="text" class="form-control" placeholder="Masukkan ID Klik Maket anda">
												</div>
												<div class="form-group">
													<label>Nama <small>(required)</small></label>
													<input name="nama" type="text" class="form-control" placeholder="Nama" value="<?= $namaIdref; ?>">
												</div>
											</div>
											<div class="col-sm-5">
												<div class="form-group">
													<label>No. KTP <small>(required)</small></label>
													<input name="no_ktp" type="number" class="form-control" placeholder="Masukkan No. KTP">
												</div>
												<div class="form-group">
													<label>Tanggal Lahir <small>(required)</small></label>
													<input type="text" data-custom-class="form-control combo-date" style="display: inline-block;" 
                                    id="date" data-format="YYYY-MM-DD" data-template="D MMM YYYY" name="tgl_lahir" value="2015-12-20">
												</div>
											</div>
										</div>
									</div>
		                            </div>
		                            <div class="tab-pane" id="account">
		                                <div class="row">
		                                    <div class="col-sm-12">
		                                        <h5 class="info-text"> 2. Informasi Data Bank </h5>
		                                    </div>
		                                    
		                                    <div class="col-sm-3 col-sm-offset-1">
		                                        <div class="form-group">
		                                            <label>Bank</label>
			                                            <select name="bank" class="form-control">
			                                                <option value="bca"> BCA </option>
			                                                <option value="bni"> BNI </option>
			                                                <option value="mandiri"> Mandiri </option>
			                                            </select>
		                                        </div>
		                                    </div>

		                                    <div class="col-sm-7 ">
		                                    	<div class="form-group">
		                                            <label>Cabang Bank</label>
		                                            <input type="text" name="cabang_bank" class="form-control" placeholder="Masukkan cabang bank anda...">
		                                        </div>
		                                    </div>
		                                    
		                                    <div class="col-sm-5 col-sm-offset-1">
		                                        <div class="form-group">
		                                            <label>Kota Bank</label>
		                                            <input type="text" name="kota_bank" class="form-control" placeholder="Masukkan kota bank anda...">
		                                        </div>
		                                    </div>
		                                    <div class="col-sm-5">
		                                        <div class="form-group">
		                                            <label>No. Rekening</label><br>
		                                            <input type="number" name="no_rekening" class="form-control" placeholder="Masukkan no rekening  anda...">
		                                        </div>
		                                    </div>

		                                    <div class="col-sm-10 col-sm-offset-1">
		                                        <div class="form-group">
		                                            <label>Nama Pemilik Rekening</label>
		                                            <input type="text" name="nama_rekening" class="form-control" placeholder="Masukkan nama rekening anda...">
		                                        </div>
		                                    </div>
		                                </div>
		                            </div>
		                            <div class="tab-pane" id="address">
		                                <div class="row">
		                                    <div class="col-sm-12">
		                                        <h5 class="info-text"> 3. Konfirmasi Pembayaran Member </h5>
		                                    </div>

		                                    <div class="col-sm-12">
		                                        <h5 class="info-text"> Harap menginformasikan pembayaran pendaftaran kepada administrator / </h5>
		                                        <h5 class="info-text"> Transfer ke STOKIST Bca Deni Santoso 788-017-4953 </h5>
		                                    </div>
		                                   
		                                </div>
		                            </div>
		                        </div>
		                        <div class="wizard-footer">
		                            <div class="pull-right">
		                                <input type='button' class='btn btn-next btn-fill btn-warning btn-wd' name='next' value='Lanjut' />
		                                <input type="submit" class='btn btn-finish btn-fill btn-warning btn-wd' name="submit" value="Selesai" />
		                            </div>

		                            <div class="pull-left">
		                                <input type='button' class='btn btn-previous btn-default btn-wd' name='previous' value='Kembali' />
		                            </div>
		                            <div class="clearfix"></div>
		                        </div>
		                    </form>
		                </div>
		            </div> <!-- wizard container -->


		        </div>
	    	</div><!-- end row -->
		</div> <!--  big container -->

	    <div class="footer">
	        <div class="container text-center">
	                                &copy;
                    2018 Supported by
                    <a href="https://www.mediahikari.com" target="_blank">Media Hikari</a>
	        </div>
	    </div>
	</div>

</body>

	<!--   Core JS Files   -->
	<script src="assets/js/jquery-2.2.4.min.js" type="text/javascript"></script>
	<script src="assets/js/bootstrap.min.js" type="text/javascript"></script>
	<script src="assets/js/jquery.bootstrap.wizard.js" type="text/javascript"></script>

	<!--  Plugin for the Wizard -->
	<script src="assets/js/paper-bootstrap-wizard.js" type="text/javascript"></script>

	<!--  More information about jquery.validate here: http://jqueryvalidation.org/	 -->
	<script src="assets/js/jquery.validate.min.js" type="text/javascript"></script>

	<script src="../view/assets/js/plugins/bootstrap-datepicker.js" type="text/javascript"></script>
	<script src='https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.13.0/moment.min.js'></script>
	<script src="../view/assets/js/plugins/combodate.js" type="text/javascript"></script>
	<script>
	$(function(){
	    $('#date').combodate({
	        minYear: 1930,
	        maxYear: 2015
	    });
	});
	</script>


</html>
