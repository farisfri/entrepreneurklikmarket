<?php
require_once "core/init.php";

//set session true/false
if(isset($_SESSION['user'])) {
    header('Location: index.php');
}else{

if(isset($_COOKIE['refe'])){
    $emailcook = $_COOKIE['refe'];
    $namecook = $_COOKIE['refename'];
}else{
    $emailcook= "daniel.director89@gmail.com";
    $namecook = "superadmin";
}

$error ='';

if(isset($_POST['submit'])){
    $username = $_POST['username'];
    $pass = $_POST['password'];
    $nama = $_POST['nama'];
    $email = $_POST['email'];
    $no_hp = $_POST['no_hp'];
    $kit_type = $_POST['kit_type'];
    $name_refer = $namecook ;
    $email_refer = $emailcook;

    if(!empty(trim($username)) && !empty(trim($pass))){
        if(register_cek($username)){
            if(register_user($username, $pass, $nama, $email, $no_hp, $kit_type, $name_refer)){ //$id_refer
                require 'account/PHPMailer/PHPMailerAutoload.php';
                $mail = new PHPMailer;
                // Konfigurasi SMTP
                $mail->isSMTP();
                $mail->Host = 'mail.entrepreneurklikmarket.com';
                $mail->SMTPAuth = true;
                $mail->Username = 'referer@entrepreneurklikmarket.com';
                $mail->Password = '1hwvyc28xmmy';
                $mail->SMTPSecure = 'ssl';
                $mail->Port = 465;

                $mail->setFrom('referer@entrepreneurklikmarket.com', 'Referensi Entrepreneur Klikmarket');
                $mail->addReplyTo('referer@entrepreneurklikmarket.com', 'Referensi Entrepreneur Klikmarket');

                // Menambahkan penerima
                $mail->addAddress($emailcook);

                // Menambahkan beberapa penerima
                //$mail->addAddress('penerima2@contoh.com');
                //$mail->addAddress('penerima3@contoh.com');

                // Menambahkan cc atau bcc
                $mail->addCC('');
                $mail->addBCC('');

                // Subjek email
                $mail->Subject = "Halo $name_refer ! Ada pendaftar baru di entrepreneurklikmarket.com";

                // Mengatur format email ke HTML
                $mail->isHTML(true);

                // Konten/isi email
                $mailContent = "<h1>Ada pendaftar baru di entrepreneurklikmarket.com dengan menggunakan ID referensi anda!</h1><br>
                    <p>Harap cek akun anda <br>Berikut data pendaftar referer anda :<br><br> Username : $username <br><br></p>";
                $mail->Body = $mailContent;

                // Menambahakn lampiran
                $mail->addAttachment('');
                $mail->addAttachment(''); //atur nama baru

                // Kirim email
                if(!$mail->send()){
                    echo 'Pesan tidak dapat dikirim.';
                    echo 'Mailer Error: ' . $mail->ErrorInfo;
                }else{
                    echo 'Pesan telah terkirim';
                }
                header('Location: index.php');
            }else{
                $error='ada masalah saat registrasi';
            }
    }else{
            $error='Username sudah ada, pilih username lain';
        }

    }else{
        $error = 'Username dan password wajib diisi';
    }
}
require_once "view/header.php";
?>

            <div class="section section-signup" >
                <div class="container">
                    <div class="row">
                        <div class="card card-signup" data-background-color="blue">
                            <span id="user-availability-status"></span>
                            <p><img src="LoaderIcon.gif" id="loaderIcon" style="display:none" /></p>
                            <form class="form" method="post" action="">
                                <div class="header header-primary text-center">
                                    <h4 class="title title-up">Daftar</h4>
                                    
                                </div>
                                
                                <div class="container content">
                                    
                                    <div class="input-group form-group-no-border">
                                        <span class="input-group-addon">
                                            <i class="now-ui-icons users_circle-08"></i>
                                        </span>
                                        <input class="form-control" placeholder="Username" onBlur="checkAvailability()" id="username" name="username" type="text" autocomplete="off" />
                                           
                                        
                                    </div>
                                    
                                    <div class="input-group form-group-no-border">
                                        <span class="input-group-addon">
                                            <i class="now-ui-icons ui-1_lock-circle-open"></i>
                                        </span>
                                        <input class="form-control" placeholder="Password" name="password" type="password" autocomplete="off" required>
                                    </div>
                                    <div class="input-group form-group-no-border">
                                        <span class="input-group-addon">
                                            <i class="now-ui-icons tech_mobile"></i>
                                        </span>
                                        <input class="form-control" placeholder="Nama" name="nama" type="text" autocomplete="off" required>
                                    </div>
                                    <div class="input-group form-group-no-border">
                                        <span class="input-group-addon">
                                            <i class="now-ui-icons ui-1_email-85"></i>
                                        </span>
                                        <input type="email" class="form-control" name="email" placeholder="Email..." required>
                                    </div>
                                    <div class="input-group form-group-no-border">
                                        <span class="input-group-addon">
                                            <i class="now-ui-icons tech_mobile"></i>
                                        </span>
                                        <input class="form-control" placeholder="No. Handphone" name="no_hp" type="text" autocomplete="off" required>
                                    </div>
                                    
                                    <div class="input-group form-group-no-border">
                                            <div class="row form-group" style="margin-left: 5px;">
                                                <div class="form-check form-check-radio" style="margin-right: 20px;">
                                                    <label class="form-check-label">
                                                        <input class="form-check-input" type="radio" name="kit_type" value="LT-1">
                                                        <span class="form-check-sign"></span>
                                                        <span style="color: #FFFFFF;">LT-1</span>
                                                    </label>
                                                </div>

                                                <div class="form-check form-check-radio" style="margin-right: 20px;">
                                                    <label class="form-check-label">
                                                        <input class="form-check-input" type="radio" name="kit_type" value="LT-2">
                                                        <span class="form-check-sign"></span>
                                                        <span style="color: #FFFFFF;">LT-2</span>
                                                    </label>
                                                </div>

                                                <div class="form-check form-check-radio" style="margin-right: 20px;">
                                                    <label class="form-check-label">
                                                        <input class="form-check-input" type="radio" name="kit_type" value="LT-3">
                                                        <span class="form-check-sign"></span>
                                                        <span style="color: #FFFFFF;">LT-3</span>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    
                                    <a href="login.php">Sudah punya akun?</a>
                                    <div id="error"><?= $error ?></div><br>
                                    <!-- If you want to add a checkbox to this form, uncomment this code -->
                                    <!-- <div class="checkbox">
                                    <input id="checkboxSignup" type="checkbox">
                                        <label for="checkboxSignup">
                                        Unchecked
                                        </label>
                                    </div> -->

                                </div>
                                <div class="footer text-center">
                                    <span id="user-availability-status"></span>
                                    <button class="btn btn-simple btn-white btn-round btn-lg" name="submit" id="submit" type="submit">Daftar</button>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="col text-center">
                        <a href="index.php" class="btn btn-simple btn-round btn-white btn-lg">Home</a>
                    </div>
                </div>
            </div>
    


<?php 
require_once "view/footer.php";
} ?>