<?php

require_once "core/init.php";

$rand = get_rand_id(32);

//set session true/false

if(isset($_SESSION['user'])) {
       header('Location: account/index.php');
}else{
$error ='';

if(isset($_POST['submit'])){
    $email = $_POST['email'];

    if(!empty(trim($email))){
        if(email_cek($email)){
          $error='<div class="alert alert-danger" role="alert">Email tidak terdaftar</div>';
        }else{
          //Rules rand & email

          if(send_userPass($rand, $email)){
            require 'account/PHPMailer/PHPMailerAutoload.php';

              $mail = new PHPMailer;

              // Konfigurasi SMTP

              $mail->isSMTP();

              $mail->Host = 'mail.entrepreneurklikmarket.com';

              $mail->SMTPAuth = true;

              $mail->Username = 'forgot-password@entrepreneurklikmarket.com';

              $mail->Password = '1hwvyc28xmmy';

              $mail->SMTPSecure = 'ssl';

              $mail->Port = 465;

              $mail->setFrom('forgot-password@entrepreneurklikmarket.com', 'Lupa Password Entrepreneur Klikmarket');

              $mail->addReplyTo('forgot-password@entrepreneurklikmarket.com', 'Lupa Password Entrepreneur Klikmarket');

              // Menambahkan penerima

              $mail->addAddress($email);

              // Menambahkan beberapa penerima

              //$mail->addAddress('penerima2@contoh.com');

              //$mail->addAddress('penerima3@contoh.com');

              // Menambahkan cc atau bcc

              $mail->addCC('');

              $mail->addBCC('');
              // Subjek email

              $mail->Subject = 'Permintaan perubahan password entrepreneurklikmarket.com';

              // Mengatur format email ke HTML

              $mail->isHTML(true);
              // Konten/isi email

              $mailContent = "<h1>Lupa Password anda?</h1><br>

                  <p>Klik link berikut untuk memulihkan password anda! <br><br> Anda dapat login ke <a>www.entrepreneurklikmarket.com/change-password.php?TokenID=$rand</a></p>";

              $mail->Body = $mailContent;
              // Menambahakn lampiran
              $mail->addAttachment('');
              $mail->addAttachment(''); //atur nama baru
              // Kirim email
              if(!$mail->send()){
                  echo 'Pesan tidak dapat dikirim.';
                  echo 'Mailer Error: ' . $mail->ErrorInfo;
              }else{
                echo 'Pesan telah terkirim';
              }
              header('Location: index.php');
          }else {
            $error='<div class="alert alert-danger" role="alert">Email gagal dikirim</div>';
          }
          //End Baris
        }
    }else{
        $error = '<div class="alert alert-danger" role="alert">Username dan password wajib diisi</div>';
    }
}





require_once"view/header.php";
?>
<div class="wrapper">
   <div class="section section-signup">

                <div class="container">

                    <div class="row">

                        <div class="card card-signup" data-background-color="blue">

                            <span id="user-availability-status"></span>

                            <p><img src="LoaderIcon.gif" id="loaderIcon" style="display:none" /></p>

                            <form class="form-group-no-border" method="post" action="">

                                <div class="header header-white text-center">

                                    <h4 class="title title-up">Ubah Password</h4>
                                      <?php echo $error; ?>
                                </div>
                                <div class="card-body">
                                    <div class="input-group form-group-no-border">
                                        <span class="input-group-addon">
                                            <i class="now-ui-icons users_circle-08"></i>
                                        </span>
                                        <input class="form-control" placeholder="Email anda" id="email" name="email" type="email" autocomplete="off"/>
                                    </div>

                                    <!-- If you want to add a checkbox to this form, uncomment this code -->

                                    <!-- <div class="checkbox">

                    <input id="checkboxSignup" type="checkbox">

                      <label for="checkboxSignup">

                      Unchecked

                      </label>

                      </div> -->
                                </div>
                                <div class="footer text-center">
                                    <button class="btn btn-simple btn-white btn-round btn-lg" name="submit" id="submit" type="submit">Kirim Sandi</button>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="col text-center">
                        <a href="index.php" class="btn btn-simple btn-round btn-white btn-lg">Home</a>
                    </div>
                </div>
            </div>
</div>

<?php require_once "view/footer.php" ?>

<?php } ?>

