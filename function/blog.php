<?php

function tampilkan(){
	$query = "SELECT * FROM blog ORDER BY sort ASC";
	return result($query);
}

function terendah(){
	$query = "SELECT videoke FROM blog WHERE videoke = (SELECT MIN(videoke) from blog)";
	return result($query);
}

function tertinggi(){
	$query = "SELECT videoke FROM blog WHERE videoke = (SELECT MAX(videoke) from blog)";
	return result($query);
}

function tampilkan_per_id($id){
	$query = "SELECT * FROM blog WHERE id=$id";
	return result($query);
}

function tampilkan_per_prog($progvideoku){
	$query = "SELECT * FROM blog WHERE videoke=$progvideoku";
	return result($query);
}

function tampilkan_per_video($videoke){
	$query = "SELECT * FROM blog WHERE kategori=2 AND videoke=$videoke  " ;
	return result($query);
}


function tampilkan_per_idDel($del_id){
	$query = "SELECT * FROM blog WHERE id=$del_id";
	return result($query);
}

//Pencarian
function hasil_cari($cari){
	$query = "SELECT * FROM blog WHERE judul LIKE '%$cari%'";
	return result($query);
}

function result($query){
	global $link;
	if($result = mysqli_query($link, $query) or die('gagal menampilkan data')){

		return $result;
	}
}
// end

//CRUD
function tambah_data($namafile, $judul, $konten, $tag, $kategori, $user_post, $videoke, $poin_per_video){
	$nama_file_baru = escape($namafile);
	$judul = escape($judul);
	$konten = escape($konten);
	$user_post = escape($user_post);
	$kategori = escape($kategori);
	$videoke = escape($videoke);
	$poin_per_video = escape($poin_per_video);

	$query = "INSERT INTO blog (id, nama_file ,judul, isi, tag, kategori, sort, user_post, videoke, poin_per_video) VALUES ('','$nama_file_baru','$judul','$konten','$tag','$kategori',0, '$user_post', '$videoke', '$poin_per_video')";
	return run($query);
}

function edit_data($judul, $konten, $tag, $kategori, $videoke, $poin_per_video, $id){
	$query="UPDATE blog SET judul='$judul', isi='$konten', tag='$tag', kategori='$kategori', videoke='$videoke' , poin_per_video='$poin_per_video'
	WHERE id=$id";

	return run($query);
}



function hapus_data($id){
	$query="DELETE FROM blog WHERE id=$id";
	return run($query);
}

//end of CRUD

function cek_videoprogress($videoke){
$videoke = escape(htmlentities($videoke));
$query = "SELECT videoke FROM user WHERE videoke='$videoke' ";

global $link;

	if ($result= mysqli_query($link, $query)) {
		while($row=mysqli_fetch_assoc($result)) {
			$videoke = $row['videoke'];
			return $videoke;
			}

	}
}


function run($query){
	global $link;

	if(mysqli_query($link,$query)) return true;
	else return false;
}

//Jumlah karakter yang tampil
function excerptJudul($string){
	$string = substr($string, 0, 40); //edit jumlah kata disini
	return $string . " ";
}
// end

//Jumlah karakter yang tampil
function excerpt($string){
	$string = substr($string, 0, 100); //edit jumlah kata disini
	return $string . " ";
}
// end

function escape($data){
global $link;
return mysqli_real_escape_string($link, $data);
}



?>


