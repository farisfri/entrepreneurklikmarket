<?php
require_once "core/init.php";

//set session true/false
if(isset($_SESSION['user'])) {
    header('Location: account/index.php');
}else{

$error ='';

if(isset($_POST['submit'])){
    $username = $_POST['username'];
    $pass = $_POST['password'];

    if(!empty(trim($username)) && !empty(trim($pass))){
        if(register_cek($username)){
          $error='<div class="alert alert-danger" role="alert">Username tidak ada</div>';
        }else{
          if(cek_data($username, $pass)){
            //   $_SESSION['id'] = $id;
              $_SESSION['user'] = $username;
            //   $_SESSION['nama'] = $nama;

              header('Location: account/index.php');
          }else{
              $error='<div class="alert alert-danger" role="alert">Periksa kembali username dan password anda</div>';
          }
        }
    }else{
        $error = '<div class="alert alert-danger" role="alert">Username dan password wajib diisi</div>';
    }
}


require_once"view/header.php";
?>
            <div class="section section-signup">
                <div class="container">
                    <div class="row">
                        <div class="card card-signup" data-background-color="blue">
                            <span id="user-availability-status"></span>
                            <p><img src="LoaderIcon.gif" id="loaderIcon" style="display:none" /></p>
                            <form class="form-group-no-border" method="post" action="">
                                <div class="header header-white text-center">
                                    <h4 class="title title-up">Masuk</h4>


                                      <?php echo $error; ?>

                                </div>

                                <div class="card-body">

                                    <div class="input-group form-group-no-border">
                                        <span class="input-group-addon">
                                            <i class="now-ui-icons users_circle-08"></i>
                                        </span>
                                        <input class="form-control" placeholder="Username" id="username" name="username" type="text" autocomplete="off"/>


                                    </div>

                                    <div class="input-group form-group-no-border">
                                        <span class="input-group-addon">
                                            <i class="now-ui-icons ui-1_lock-circle-open"></i>
                                        </span>
                                        <input class="form-control" placeholder="Password" name="password" type="password" autocomplete="off" required>
                                    </div>

                                    <a href="forgot-password.php">Lupa Password?</a>
                                    <!-- If you want to add a checkbox to this form, uncomment this code -->
                                    <!-- <div class="checkbox">
	  								<input id="checkboxSignup" type="checkbox">
	  									<label for="checkboxSignup">
	  									Unchecked
	  									</label>
	  						  		</div> -->

                                </div>
                                <div class="footer text-center">
                                    <button class="btn btn-simple btn-white btn-round btn-lg" name="submit" id="submit" type="submit">Login</button>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="col text-center">
                        <a href="index.php" class="btn btn-simple btn-round btn-white btn-lg">Home</a>
                    </div>
                </div>
            </div>



<?php require_once "view/footer.php" ?>

<?php } ?>
