
moment.locale('en');
var $combodateInput = $('#date');

$combodateInput
  .attr('data-max-year',getYear)
  .combodate({firstItem:'name'});

// Get current year - Used in footer and date of birth datepicker
// -----------------------------------------
function getYear(){
    var currentDate = new Date();
    var currentYear = currentDate.getFullYear();
    return currentYear;
}

// remove the disabled label option
 var monthLength = $('select.month> option').length - 1;


for (var i=0;i<monthLength;i++){
   $('select.month> option[value="'+i+'"]').text(months[i]);
} 

//arabic 
//-----------------------
/* 
day = يوم
month = شهر
months = [
        'كانون الثاني يناير',
        'شباط فبراير',
        'آذار مارس',
        'نيسان أبريل',
        'أيار مايو',
        'حزيران يونيو',
        'تموز يوليو',
        'آب أغسطس',
        'أيلول سبتمبر',
        'تشرين الأول أكتوبر',
        'تشرين الثاني نوفمبر',
        'كانون الأول ديسمبر'
    ]; */