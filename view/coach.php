 <?php
        $coach=lihatCoach();
        
   ?>


    <div class="wrapper">
        <div class="landing-page section-sub section-team text-center ">
            <div class="container">
                <h2 class="title">Executive Coach</h2>
                <div class="team">
                    <div class="row">
                      <?php while($row=mysqli_fetch_assoc($coach)):  ?>
                        <div class="col-md-3">
                            <div class="team-player">
                                <a href="account/<?= $row['nama_file'];?>" data-lightbox="image-1" data-title="Executive Coach">
                                    <img src="account/<?= $row['nama_file'];?>" style="width: 180px; height: 180px; object-fit: cover;"  alt="Executive Coach EA" class="rounded img-fluid img-raised">
                                </a>
                                <h4 class="title"><?= $row['nama']; ?></h4>
                                <p class="category text-primary"><?= $row['kota']; ?></p>
                                <p class="description"></p>
                                <!--<a href="#pablo" class="btn btn-primary btn-icon btn-icon-mini"><i class="fa fa-twitter"></i></a>
                                <a href="#pablo" class="btn btn-primary btn-icon btn-icon-mini"><i class="fa fa-instagram"></i></a>
                                <a href="#pablo" class="btn btn-primary btn-icon btn-icon-mini"><i class="fa fa-facebook-square"></i></a>-->
                            </div>
                        </div>
                        <?php endwhile; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>

