
        <div class="section section-download" id="#download-section" data-background-color="black">
            <div class="container">
                <!--<div class="row justify-content-md-center">
                    <div class="text-center col-md-12 col-lg-8">
                        <h3 class="title">Do you love this Bootstrap 4 UI Kit?</h3>
                        <h5 class="description">Cause if you do, it can be yours for FREE. Hit the button below to navigate to Creative Tim or Invision where you can find the kit in HTML or Sketch/PSD format. Start a new project or give an old Bootstrap project a new look!</h5>
                    </div>
                    <div class="text-center col-md-12 col-lg-8">
                        <a href="" class="btn btn-primary btn-lg btn-round" role="button">
                            Daftar Disini
                        </a>
                    </div>
                </div>-->
                
                <div class="row sharing-area">
                    <div class="col-md-12 text-center">
                        <h3 class="title">Social Media!</h3>
                    </div>
                    <div class="row col-md-12">
                        <div class="col-md-4 icon-sosmed">
                            <ul>
                                <li>
                                    <a target="_blank" href="https://www.instagram.com/lifetimeofficial/" >
                                        <i class="fab fa-instagram fa-lg"></i>
                                        <span>lifetimeofficial</span>
                                    </a>
                                </li>
                                <li>

                                    <a target="_blank" href="https://www.instagram.com/klikmarket.id/">
                                        <i class="fab fa-instagram fa-lg"></i>
                                    
                                        <span>klikmarket.id</span>
                                    </a>
                                </li>
<!--                                 <li>

                                    <a target="_blank" href="https://www.instagram.com/klikrajavoucher/">
                                        <i class="fab fa-instagram fa-lg"></i>
                                    
                                    <span>klikrajavoucher</span>
                                    </a>

                                </li> -->

                            </ul>
                        </div>
                        <div class="col-md-4">

                             <ul>
                                <li>
                                    <a target="_blank" href="https://www.instagram.com/entrepreneuracademy10/">
                                        <i class="fab fa-instagram fa-lg"></i>
                                    
                                        <span>entrepreneuracademy10</span>
                                    </a>
                                </li>
                                <li>
                                    <a target="_blank" href="https://www.instagram.com/ea.klikmarket/">
                                        <i class="fab fa-instagram fa-lg"></i>
                                    
                                        <span>ea.klikmarket</span>
                                    </a>
                                </li>

                                <!--<li>

                                    <a target="_blank" href="#">
                                        <i class="fab fa-line fa-lg"></i>
                                    
                                    <span>@itslifetime.com</span>
                                    </a>
                                </li>
                                <li>
                                    <a target="_blank" href="#">
                                        <i class="fab fa-line fa-lg"></i>
                                    
                                    <span>@klikmarket</span>
                                    </a>
                                </li>
                                <li>
                                    <a target="_blank" href="#">
                                        <i class="fab fa-line fa-lg"></i>
                                    
                                        <span>@klikrajavoucher</span>
                                    </a>
                                </li>-->
                            </ul> 
                        </div>
                        <div class="col-md-4">
                            <ul >
                                
                                <li>
                                    <a target="_blank" href="https://www.youtube.com/channel/UCaoY8KVjtOSV47coCRMbH5A">
                                        <i class="fab fa-youtube fa-lg"></i>
                                    
                                    <span>lifetimeworld</span>
                                    </a>
                                </li>

                                <li>
                                    <a target="_blank" href="https://www.youtube.com/channel/UCzYqFm1UQ0gT29cVI1CglNg">
                                        <i class="fab fa-youtube fa-lg"></i>
                                    
                                        <span>entrepreneuracademy</span>
                                    </a>
                                </li>
                                
<!--                                 <li>

                                    <a target="_blank" href="#">
                                        <i class="fab fa-line fa-lg"></i>
                                    
                                        <span>085604448877</span>
                                    </a>

                                </li> -->

                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <footer class="footer" data-background-color="black">
            <div class="container">
                <nav>
                    <ul><!--
                        <li>
                            <a href="faq.php">
                                FAQ
                            </a>
                        </li>
                        <li>
                            <a href="relawan.php">
                                Relawan
                            </a>
                        </li>
                        <li>
                            <a href="contact.php">
                                Kontak Kami
                            </a>
                        </li>
                    </ul>-->
                </nav>
                <div class="copyright">
                    &copy;
                    2018 Entrepreneur Klik Market <!--Supported by
                    <a href="https://www.mediahikari.com" target="_blank">Media Hikari</a>-->
                </div>
            </div>
        </footer>

</body>



<!--   Core JS Files   -->

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery.touchswipe/1.6.4/jquery.touchSwipe.min.js"></script>
    <script type="text/javascript">
    $(".carousel").swipe({

      swipe: function(event, direction, distance, duration, fingerCount, fingerData) {

        if (direction == 'left') $(this).carousel('next');
        if (direction == 'right') $(this).carousel('prev');

      },
      allowPageScroll:"vertical"

    });
</script>
<script src="./view/assets/js/core/popper.min.js" type="text/javascript"></script>
<script src="./view/assets/js/core/bootstrap.min.js" type="text/javascript"></script>
<!--  Plugin for Switches, full documentation here: http://www.jque.re/plugins/version3/bootstrap.switch/ -->
<script src="./view/assets/js/plugins/bootstrap-switch.js"></script>
<!--  Plugin for the Sliders, full documentation here: http://refreshless.com/nouislider/ -->
<script src="./view/assets/js/plugins/nouislider.min.js" type="text/javascript"></script>
<!--  Plugin for the DatePicker, full documentation here: https://github.com/uxsolutions/bootstrap-datepicker -->
<script src="./view/assets/js/plugins/bootstrap-datepicker.js" type="text/javascript"></script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.13.0/moment.min.js'></script>
<script src="./view/assets/js/plugins/combodate.js" type="text/javascript"></script>
<script src="./view/assets/js/lightbox-plus-jquery.min.js"></script>
<script>
$(function(){
    $('#date').combodate({
        minYear: 1930,
        maxYear: 2015
    });
});
</script>

<script type="text/javascript">
    $('.carousel').carousel({
      interval: 5000
    })

</script>

<script>
    lightbox.option({
      'maxWidth' : 500,
    })
</script>

<!-- Control Center for Now Ui Kit: parallax effects, scripts for the example pages etc -->
<script src="./view/assets/js/now-ui-kit.js?v=1.1.0" type="text/javascript"></script>
<script src="account/player/build/mediaelement-and-player.min.js"></script>
<script type="text/javascript">
$('a[href^="#"]').on('click', function(event) {

    var target = $( $(this).attr('href') );

    if( target.length ) {
        event.preventDefault();
        $('html, body').animate({
            scrollTop: target.offset().top
        }, 1000);
    }

});</script>

<script type="text/javascript">
    function formatMoney(cost) {
        var label = "";
        if (cost > 999999999) {
            cost = Math.round(cost / 1000000000 * 100) / 100;
            label = " M";
        }
        else if (cost > 999999) {
            cost = Math.round(cost / 1000000 * 100) / 100;
            label = " JT";
        }
        return "Rp " + cost + label;
    }

    function updateCount() {
        var items = $('.section-1 .item.select');
        var n = items.length;
        $('.section-1 .choose-label span').html('pilih '+ (3-n) +' lagi');

        var totalCost = 0;
        for (var i = 0; i< items.length; i++) {
            var item = items[i];
            var cost = $(item).attr("data-cost") * 1;
            totalCost += cost;

            
            $('#impian-'+(i+1)+' .pricetag').html(formatMoney(cost));
            $('#impian-'+(i+1)+' img').attr("src", $(item).find('img').attr("src"));
        }

        $('#impian-total').html("TOTAL = "  + formatMoney(totalCost));


        var duration = Math.floor(totalCost / 5000000 / 12 * 10) * 0.1;
        $('#impian-duration span').html(duration);


        var inflasi = totalCost;
        for (var i = 0; i< duration; i++) {
            inflasi *= 1.1;
        }
        inflasi = Math.round(inflasi);

        var msg = "asumsi inflasi 10% per tahun maka nilai <strong>"+formatMoney(totalCost)+"</strong> sekarang adalah <strong>" + formatMoney(inflasi) + "</strong> di "+duration+" tahun kemudian"
        $('#impian-inflasi').html(msg);
        
        console.log("total cost", totalCost);
    }
    updateCount();

    $('.section-1 .item').click(function() {
        var n = $('.section-1 .item.select').length;
        if ((n < 3 && !$(this).hasClass("select")) || $(this).hasClass('select'))
            $(this).toggleClass("select");

        updateCount();
        console.log();
    });
</script>


</html>
