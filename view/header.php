<?php

$login = false;

if(isset($_SESSION['user'])) {
    $login = true;
    $usid = $_SESSION['user'];
    $usRef = tampilUsername($usid);
    while($row=mysqli_fetch_assoc($usRef)){
        $userIdRef = $row['id'];
        $usernameIdRef = $row['username'];
        $emailIdRef = $row['email'];
        $namaIdref = $row['nama'];
    }  
}

if(isset($_GET['ref'])){
    if(referer_cek($_GET['ref'])){
        echo "Tidak ada referer";
    }else{
        $reference =tampilRef($_GET['ref']);
        while($row=mysqli_fetch_assoc($reference)){
            $idRef = $row['id'];
            $usernameRef = $row['username'];
            $emailRef = $row['email'];
            setcookie("refename", $usernameRef, time()+24*60*60);
            setcookie("refe", $emailRef, time()+24*60*60);
        }
    }
}



if(isset($_COOKIE['refe'])){
    $emailcook = $_COOKIE['refe'];
    $namecook = $_COOKIE['refename'];
}else{
    $emailcook= "daniel.director89@gmail.com";
    $namecook = "superadmin";
}


?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <link rel="apple-touch-icon" sizes="80x80" href="./view/assets/img/80x80.png">
    <link rel="icon" type="image/png" href="./view/assets/img/80x80.png">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <title>Entrepreneur Klik Market</title>
    <meta name="keywords" content="entrepreneur academy,entrepreneurklikmarket.com,entrepreneur klik market,klikmarket,klik raja voucher,entrepreneurklikmarket">
     <meta name="description" content="Website entrepreneur klik market">
    <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no" name="viewport" />
    <!-- URL Theme Color untuk Chrome, Firefox OS, Opera dan Vivaldi -->
    <meta name="theme-color" content="#FFFFFF" />
    <!-- URL Theme Color untuk Windows Phone -->
    <meta name="msapplication-navbutton-color" content="#FFFFFF" />
    <!-- URL Theme Color untuk iOS Safari -->
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="apple-mobile-web-app-status-bar-style" content="#FFFFFF" />
    <!--     Fonts and icons     -->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700,200" rel="stylesheet" />
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
    <!-- CSS Files -->
    <link href="./view/assets/css/bootstrap.min.css" rel="stylesheet" />
    <link href="./view/assets/css/now-ui-kit.css?v=1.1.0" rel="stylesheet" />
    <!-- CSS Just for demo purpose, don't include it in your project -->
    <link href="./view/assets/css/demo.css" rel="stylesheet" />
    <link rel="stylesheet" href="account/player/build/mediaelementplayer.min.css" />
    <link rel="stylesheet" href="./view/assets/css/lightbox.min.css">
    
</head>

<body class="index-page sidebar-collapse">
    <!-- Navbar -->
     <nav class="navbar navbar-expand-lg bg-white fixed-top navbar-white">
        <div class="container">
            <div class="navbar-translate">
                <a class="navbar-brand" href="index.php" rel="tooltip" title="Home" data-placement="bottom">
                   <img src="view/assets/img/logoen.png" class="img-fluid" width="50%">
                </a>
                <button class="navbar-toggler navbar-toggler" type="button" data-toggle="collapse" data-target="#navigation" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-bar bar1"></span>
                    <span class="navbar-toggler-bar bar2"></span>
                    <span class="navbar-toggler-bar bar3"></span>
                </button>
            </div>
            <div class="collapse navbar-collapse justify-content-end" id="navigation">
                <ul class="navbar-nav">

                    <!--<li class="nav-item">
                        <a class="nav-link" href="#subscribe">
                            <i class="now-ui-icons arrows-1_cloud-download-93"></i>
                            <p>Download</p>
                        </a>
                    </li>-->
                    <li class="nav-item">
                        <a class="nav-link" href="aktivitas.php">
                            <i class="fas fa-info fa-lg"></i>
                            <p>Aktivitas</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="coach.php">
                            <i class="fas fa-user-plus fa-lg"></i>
                            <p>Executive Coach</p>
                        </a>
                    </li>

                    <!-- <li class="nav-item">
                        <a class="nav-link" href="daftar.php">
                            <i class="fas fa-pencil-square-o fa-lg"></i>
                            <p>Daftar</p>
                        </a>
                    </li> -->

                    <li class="nav-item">
                        <a class="nav-link" href="login.php">
                            <i class="fas fa-user-circle-o fa-lg"></i>
                            <p>Login</p>
                        </a>
                    </li>
<!--                <li class="nav-item dropdown">
                      <a href="login.php" class="nav-link dropdown-toggle" data-toggle="dropdown">
                        <i class="fas fa-user-circle-o fa-lg"></i>
                            <p>Login</p>
                            <b class="caret"></b>
                      </a>
                      <ul class="dropdown-menu">
                        <li><a class="nav-link" href="login.php">LOGIN</a></li>
                        <li><a class="nav-link" href="http://www.itslifetime.com/member/login" target="_blank">LOGIN LIFETIME</a></li>
                        <li><a class="nav-link" href="http://lifetimeshare.com/member/index.php?r=site%2Flogin" target="_blank">LOGIN POINT SHARE</a></li>
                      </ul>
                    </li> -->
                </ul>
            </div>
        </div>
    </nav>

    <!-- End Navbar -->